# X-Cart tools

Allows you to perform various development actions from CLI. Current list of commands is:

  - `admin` - Creates administrator profile using X-Cart DB connection
  - `config` - Creates X-Cart config by asking you a few questions
  - `deploy` - Deployes X-Cart from different sources. Available sources are repo, latest version and external server.
  - `doctor` - Checks & fixes X-Cart 5 store
  - `dump` - Creates database dump. Can be run on remote host. Use -m param to set dump mode.
  - `module` - Creates new empty module. Can be run on remote host.
  - `pack` - Creates module package by specified AuthorID-ModuleId name
  - `product` - Generates product entities into X-Cart DB
  - `redeploy` - Recreates X-Cart code cache and applies migrations\hooks
  - `standUpload` - [BETA] Uploads X-Cart project to stand. Not for production usage
  
## Global command parameters

- `-s, --ssh_string[=SSH_STRING]` -  SSH user@host string to use in command
- `-P, --ssh_port[=SSH_PORT]` - SSH port [default: 22]

## Example

Change working directory to the X-Cart installation or development repository and run:

```bash
> php <path-to-tools>/tools.php <command>

# e.g.
> php <path-to-tools>/tools.php redeploy
```

### Remote command execution

To execute a command via SSH, you must have your public key on the remote server, and then you can run command as in example:

```bash
> tools <command> -s remote_user@remote_host
```

For instance, `dump` command will create a dump on the remote server and then automatically download it to the local machine.

## How to install globally via Composer

Add the following line to your ~/.bash_profile script:

```bash
export PATH="$HOME/.composer/vendor/bin:$PATH"
```

And then simply run

```bash
> composer global require xcart/xcart-tools
```

After that the tools will be available in CLI as
```bash
> tools <command>

# e.g.
> tools redeploy
```

To view help on every command run

```bash 
> tools <command> -h

#e.g.
> tools admin -h
```

## Commands description and examples

### admin

Creates administrator profile using X-Cart DB connection. 

Supports execution via SSH.
```bash
> tools admin

# with parameters
> tools admin -u admin@example.com -p master2356

# via SSH
> tools admin -u admin@example.com -p master2356 -s user@remote.com -P 22

# remove
> tools admin -u admin@example.com --remove=1
```

### config
Creates X-Cart config by asking you a few questions.

Supports execution via SSH.
```bash
> tools config

# with parameters
> tools config -d my-store.com -w my_store

# via SSH
> tools config -s user@remote.com
```

### deploy
Deployes X-Cart from different sources. Available sources are repo, latest version and external server. 
Command interacts with user by serveral questions
 
```bash
# deploy from latest or repo
> tools deploy

# deploy from remote server
> tools deploy -s user@remote.com
```

### doctor
Checks & fixes X-Cart 5 store

The only action is available now is 'clear_rebuild'. It clears all the temporary files created by crashed rebuild process.

```bash

> tools doctor

# with parameters
> tools doctor -m clear_rebuild
```

### dump
Creates database dump. Can be run on remote host. Use -m param to set dump mode.

Supports execution via SSH.
```bash
> tools dump

# dump from remote server
> tools dump -s user@remote.com
```

### module
Creates new empty module. Can be run on remote host.

Command interacts with user by serveral questions

Supports execution via SSH.
```bash
> tools module

# create new module on remote server
> tools module -s user@remote.com
```

### pack
Creates module package by specified AuthorID-ModuleId name

```bash
> tools pack <AuthorId>-<ModuleId>

# e.g.
> tools pack Qualiteam-MyModule
```

### product
Generates product entities into X-Cart DB

```bash
> tools product

> tools product -a 5
```

### redeploy
Recreates X-Cart code cache and applies migrations\hooks

```bash
> tools redeploy
```
