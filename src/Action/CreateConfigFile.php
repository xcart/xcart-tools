<?php
/**
 * Copyright (c) 2011-present Qualiteam software Ltd. All rights reserved.
 * See https://www.x-cart.com/license-agreement.html for license details.
 */

namespace XcartTools\Action;

use XcartTools\Context\ContextInterface;
use XcartTools\Utils\XcartPathLocator;

class CreateConfigFile implements ActionInterface
{
    /**
     * @var XcartPathLocator
     */
    private $pathLocator;

    public function __construct(XcartPathLocator $pathLocator)
    {
        $this->pathLocator = $pathLocator;
    }

    /**
     * @param ContextInterface $context
     * @param array            $options
     *
     * @return string
     */
    public function execute(ContextInterface $context, $options)
    {
        $content = $this->buildConfigFile($options);
        $context->write($this->pathLocator->getConfigFilepath(), $content);

        return $this->pathLocator->getConfigFilepath();
    }

    /**
     * @param array $options
     *
     * @return string
     */
    protected function buildConfigFile($options)
    {
        $dbSocket = isset($options['dbSocket']) ? $options['dbSocket'] : '';
        $dbPort   = isset($options['dbPort']) ? $options['dbPort'] : '';
        $dbName   = isset($options['dbName']) ? $options['dbName'] : '';
        $dbUser   = isset($options['dbUser']) ? $options['dbUser'] : '';
        $dbPass   = isset($options['dbPass']) ? $options['dbPass'] : '';

        $domain   = isset($options['domain']) ? $options['domain'] : '';
        $webdir   = isset($options['webdir']) ? $options['webdir'] : '';

        $authCode  = isset($options['authCode']) ? $options['authCode'] : '';
        $secretKey = isset($options['secretKey']) ? $options['secretKey'] : '';
        $devMode   = (isset($options['devMode']) && $options['devMode']) ? 'On' : 'Off';

        return <<<CONFIG
; <?php /*
; WARNING: Do not change the line above
;
; +-------------------------------------+
; |   X-Cart 5 configuration file   |
; +-------------------------------------+
;
; -----------------
;  About this file
; -----------------
;

;
; ----------------------
;  SQL Database details
; ----------------------
;
[database_details]
hostspec = "localhost"
socket = "${dbSocket}"
port = "${dbPort}"
database = "${dbName}"
username = "${dbUser}"
password = "${dbPass}"
table_prefix = "xc_"

;
; ----------------------
;  Cache settings
; ----------------------
;
[cache]
; Type of cache used. Can take auto, memcache, memcached, apc, xcache, file values.
type=auto
; Cache namespace
namespace=XLite
; List of memcache servers. Semicolon is used as a delimiter.
; Each server is specified with a host name and port number, divided
; by a colon. If the port is not specified, the default
; port 11211 is used.
servers=

;
; -----------------------------------------------------------------------
;  X-Cart 5 HTTP & HTTPS host, web directory where cart installed
;  and allowed domains
; -----------------------------------------------------------------------
;
; NOTE:
; You should put here hostname ONLY without http:// or https:// prefixes.
; Do not put slashes after the hostname.
; Web dir is the directory in the URL, not the filesystem path.
; Web dir must start with slash and have no slash at the end.
; The only exception is when you configure for the root of the site,
; in which case you should leave the option empty.
; Domains should be listed separated by commas.
;
; WARNING: Do not set the "$" sign before the parameter names!
;
; EXAMPLE 1:
;
;   http_host = "www.yourhost.com"
;   https_host = "www.securedirectories.com/yourhost.com"
;   web_dir = "/shop"
;   domains = "www.yourhost2.com,yourhost3.com"
;
; will result in the following URLs:
;
;   http://www.yourhost.com/shop
;   https://www.securedirectories.com/yourhost.com/shop
;
;
; EXAMPLE 2:
;
;   http_host = "www.yourhost.com"
;   https_host = "www.yourhost.com"
;   web_dir = ""
;
; will result in the following URLs:
;
;   http://www.yourhost.com
;   https://www.yourhost.com
;
[host_details]
http_host = "${domain}"
https_host = "${domain}"
web_dir = "/${webdir}"
domains = ""
admin_self = "admin.php"
cart_self = "cart.php"
;
; -----------------
;  Logging details
; -----------------
;
[log_details]
type = file
name = "var/log/xlite.log.php"
level = LOG_DEBUG
ident = "XLite"
suppress_errors = Off
suppress_log_errors = Off

;
; Installer authcode.
; A person who do not know the auth code can not access the installation script.
; Installation authcode is created authomatically and stored in this section.
;
[installer_details]
auth_code = "${authCode}"
shared_secret_key = "${secretKey}"

;
; Some options to optimize the store
;
[performance]
developer_mode = ${devMode}
ignore_system_modules = On

[service]
verify_certificate = Off

[trial]
end_date = 01-01-2999

; */ ?>
CONFIG;
    }
}