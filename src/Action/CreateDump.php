<?php
/**
 * Copyright (c) 2011-present Qualiteam software Ltd. All rights reserved.
 * See https://www.x-cart.com/license-agreement.html for license details.
 */

namespace XcartTools\Action;

use DateTimeZone;
use XcartTools\Command\CreateDumpCommand;
use XcartTools\Command\SqlOutputHelperTrait;
use XcartTools\Context\ContextInterface;
use XcartTools\Context\LocalContext;
use XcartTools\Context\RemoteOverSshContext;
use XcartTools\Domain\Provider\Database;
use XcartTools\Domain\Provider\ModuleInfo;
use XcartTools\Repository\Attributes;
use XcartTools\Repository\AttributesOptions;
use XcartTools\Repository\AttributeValuesSelect;
use XcartTools\Repository\Categories;
use XcartTools\Repository\CategoryProducts;
use XcartTools\Repository\GlobalProductTabs;
use XcartTools\Repository\Languages;
use XcartTools\Repository\Products;
use XcartTools\Repository\ProductTabs;
use XcartTools\Repository\ProductTranslations;
use function array_merge;
use XcartTools\Action\GetTables;


class CreateDump implements ActionInterface
{
    use SqlOutputHelperTrait;

    const MYSQL_DEFAULT_PORT = 3306;

    /**
     * @var GetTables
     */
    private $getTables;

    /**
     * Returns tables that will be ignored during dump
     *
     * @param string $mode
     *
     * @return array
     */
    public static function getIgnoredTablesList($mode = CreateDumpCommand::MODE_COPY)
    {
        switch ($mode) {
            case CreateDumpCommand::MODE_COPY:
                return [
                    'access_control_cells',
                    'access_control_entities',
                    'access_control_zones',
                    'address_field_value',
                    'commission',
                    'conversation_members',
                    'conversations',
                    'event_tasks',
                    'form_ids',
                    'import_logs',
                    'job_states',
                    'orders',
                    'order_coupons',
                    'order_details',
                    'order_history_events',
                    'order_history_event_data',
                    'order_items',
                    'order_item_attribute_values',
                    'order_item_surcharges',
                    'order_surcharges',
                    'order_tracking_number',
                    'payment_transaction_data',
                    'payment_transactions',
                    'payment_backend_transactions',
                    'payment_backend_transaction_data',
                    'product_stats',
                    'profiles',
                    'profile_addresses',
                    'profile_roles',
                    'qbo_sync_jobs_log',
                    'search_cache',
                    'sessions',
                    'session_cells',
                    'temporary_files',
                    'tmp_vars',
                    'vendor',
                    'vendor_company_field_value',
                    'vendor_convo_messages',
                    'vendor_convo_message_reads',
                    'vendor_ratings',
                    'vendor_translations',
                    'xpc_payment_data_cells',
                    'xpc_payment_fraud_check_data',
                    'xpc_payment_transaction_data'
                ];
            default:
                return [];
        }
    }

    public function __construct(GetTables $getTables)
    {
        $this->getTables = $getTables;
    }

    /**
     * @param ContextInterface $context
     *
     * @param string           $path
     *
     * @param array            $dbOptions
     *
     * @return mixed|void
     */
    public function execute(ContextInterface $context, $path, $dbOptions)
    {
        $user   = $dbOptions['user'];
        $pass   = $dbOptions['pass'];
        $prefix = $dbOptions['prefix'];
        $port   = isset($dbOptions['port']) ? $dbOptions['port'] : static::MYSQL_DEFAULT_PORT;
        $mode   = isset($dbOptions['mode']) ? $dbOptions['mode'] : CreateDumpCommand::MODE_COPY;
        $db     = $dbOptions['db'];
        $tmpPath = $context->getTmpDir() . '/dump_' . date("Ymd") . '.sql';

        // Dump schema
        $dumpSchemaCmd = array_merge(
            $this->getDumpCmd($user, $pass, $port),
            ['--no-data'],
            $this->getNotXcartTables($db, $prefix, $context, $dbOptions),
            [$db],
            ['>', $tmpPath]
        );

        $context->exec($dumpSchemaCmd);

        // Dump data
        $dumpDataCmd = array_merge(
            $this->getDumpCmd($user, $pass, $port),
            ['--no-create-info'],
            $this->getIgnoredTables($mode, $db, $prefix),
            $this->getNotXcartTables($db, $prefix, $context, $dbOptions),
            $this->getDatabaseAndTables($db, $mode),
            ['>>', $tmpPath]
        );

        $context->exec($dumpDataCmd);

        if ($context instanceof RemoteOverSshContext) {
            $context->download($tmpPath, $path);
            $context->remove($tmpPath);
        } elseif ($context instanceof LocalContext) {
            $context->move($tmpPath, $path);
        }

        return realpath($path);
    }

    /**
     * @param string $user
     * @param string $pass
     * @param string $port
     *
     * @return array
     */
    protected function getDumpCmd($user, $pass, $port)
    {
        return [
            'mysqldump',
            '--single-transaction=TRUE',
            '--user="' . $user . '"',
            '--password="' . $pass . '"',
            '--port=' . $port,
            '--add-drop-table',
        ];
    }

    /**
     * @param string $database
     *
     * @param string $mode
     *
     * @return array
     */
    protected function getDatabaseAndTables($database, $mode)
    {
        return [$database];
    }

    /**
     * @param string $mode
     * @param string $db
     * @param string $prefix
     *
     * @return array
     */
    protected function getIgnoredTables($mode, $db, $prefix = '')
    {
        $tables = static::getIgnoredTablesList($mode);

        return array_map(function ($item) use ($db, $prefix) {
            return '--ignore-table=' . $db . '.' . $prefix . $item;
        }, $tables);
    }

    /**
     * @param string $mode
     * @param string $db
     * @param string $prefix
     *
     * @return array
     */
    protected function getNotXcartTables($db, $prefix, ContextInterface $context, $dbOptions)
    {
        if (!$dbOptions['xc5_only']) {
            return [];
        }

        $tables = $this->sqlColumnToArray($this->getTables->execute($context, $dbOptions));

        return array_filter(array_map(function ($item) use ($db) {
            return (!preg_match('/^xc_/', $item)) ? ('--ignore-table=' . $db . '.'  . $item) : '';
        }, $tables), 'strlen');
    }
}