<?php
/**
 * Copyright (c) 2011-present Qualiteam software Ltd. All rights reserved.
 * See https://www.x-cart.com/license-agreement.html for license details.
 */

namespace XcartTools\Action;

use DateTimeZone;
use XcartTools\Domain\Provider\Database;
use XcartTools\Domain\Provider\ModuleInfo;
use XcartTools\Repository\Attributes;
use XcartTools\Repository\AttributesOptions;
use XcartTools\Repository\AttributeValuesSelect;
use XcartTools\Repository\Categories;
use XcartTools\Repository\CategoryProducts;
use XcartTools\Repository\GlobalProductTabs;
use XcartTools\Repository\Languages;
use XcartTools\Repository\Products;
use XcartTools\Repository\ProductTabs;
use XcartTools\Repository\ProductTranslations;

class CreateProducts implements ActionInterface
{
    /**
     * @var Database
     */
    private $database;

    /**
     * @var ModuleInfo
     */
    private $moduleInfoProvider;

    /**
     * @var Categories
     */
    private $categories;

    /**
     * @var CategoryProducts
     */
    private $categoryProducts;

    /**
     * @var Attributes
     */
    private $attributes;

    /**
     * @var AttributesOptions
     */
    private $attributeOptions;

    /**
     * @var AttributeValuesSelect
     */
    private $attributeValuesSelect;

    /**
     * @var Products
     */
    private $products;

    /**
     * @var ProductTranslations
     */
    private $productTranslations;

    /**
     * @var ProductTabs
     */
    private $productTabs;

    /**
     * @var GlobalProductTabs
     */
    private $globalProductTabs;

    /**
     * @var Languages
     */
    private $languages;

    public function __construct(
        Database $database,
        ModuleInfo $moduleInfoProvider,
        Categories $categories,
        CategoryProducts $categoryProducts,
        Attributes $attributes,
        AttributesOptions $attributeOptions,
        AttributeValuesSelect $attributeValuesSelect,
        Products $products,
        ProductTranslations $productTranslations,
        ProductTabs $productTabs,
        GlobalProductTabs $globalProductTabs,
        Languages $languages
    ) {
        $this->database              = $database;
        $this->moduleInfoProvider    = $moduleInfoProvider;
        $this->categories            = $categories;
        $this->categoryProducts      = $categoryProducts;
        $this->attributes            = $attributes;
        $this->attributeOptions      = $attributeOptions;
        $this->attributeValuesSelect = $attributeValuesSelect;
        $this->products              = $products;
        $this->productTranslations   = $productTranslations;
        $this->productTabs           = $productTabs;
        $this->globalProductTabs     = $globalProductTabs;
        $this->languages             = $languages;
    }

    /**
     * @param integer $qty
     *
     * @return mixed|void
     * @throws \Exception
     */
    public function execute($qty)
    {
        $qty = (int) $qty <= 0 ? 1 : (int) $qty;

        $langs      = $this->languages->findAll();
        $globalTabs = $this->globalProductTabs->findAll();
        $xData      = $this->getProductXData();
        $categories = $this->categories->findAll();
        $attributes = $this->attributes->findAll();
        $attrIds    = [];

        foreach ($attributes as $attr) {
            $attrIds[] = $attr['id'];
        }

        $attributesOptions = $this->attributeOptions->findApplicable($attrIds);

        $created = 0;

        for ($i = 0; $i < $qty; $i++) {
            $connection = $this->database->getConnection();
            $connection->beginTransaction();
            $price = $this->getProductRandPrice();

            $addition = [];
            if ($this->moduleInfoProvider->getModuleInfo('CDev\MarketPrice')) {
                $addition['marketPrice'] = 0;
            }
            if ($this->moduleInfoProvider->getModuleInfo('CDev\Sale')) {
                $rnd            = rand(1, 100);
                $salePriceValue = 0;
                if ($rnd <= 10) {
                    $salePriceValue = $this->getProductRandPrice(0, $price);
                }
                $addition['discountType']   = 'sale_price';
                $addition['salePriceValue'] = $salePriceValue;
            }
            if ($this->moduleInfoProvider->getModuleInfo('XC\FreeShipping')) {
                $addition['freightFixedFee'] = 0;
            }
            if ($this->moduleInfoProvider->getModuleInfo('XC\FacebookMarketing')) {
                $addition['facebookMarketingEnabled'] = 1;
            }
            $dateAdded = $this->time();

            try {
                $productId = $this->products->insert(array_merge([
                    'entityVersion' => $this->generateUUIDv4(),
                    'sku'           => 'temporary',
                    'price'         => $price,
                    'enabled'       => 1,
                    'weight'        => 0,
                    'boxWidth'      => 0,
                    'boxLength'     => 0,
                    'boxHeight'     => 0,
                    'itemsPerBox'   => 1,
                    'taxable'       => 1,
                    'arrivalDate'   => $dateAdded,
                    'date'          => $dateAdded,
                    'updateDate'    => $dateAdded,
                    'attrSepTab'    => 1,
                    'metaDescType'  => 'A',
                ], $addition));

                $sku = $this->prepareProductXName($xData['sku'], $productId);
                $this->products->updateById($productId, 'sku', $sku);

                foreach ($langs as $lng) {
                    if (!$lng['enabled']) {
                        continue;
                    }
                    $name             = $this->prepareProductXName($xData['name'], $productId);
                    $briefDescription = $this->prepareProductXName($xData['briefDescription'], $productId);
                    $description      = $this->prepareProductXName($xData['description'], $productId);
                    $this->productTranslations->insert([
                        'id'               => $productId,
                        'code'             => $lng['code'],
                        'name'             => $name,
                        'briefDescription' => $briefDescription,
                        'description'      => $description,
                    ]);
                }

                $globalTabsInversePosition = [];
                $globalTabsLength          = count($globalTabs);
                foreach ($globalTabs as $k => $tab) {
                    $tab['position']             = -($globalTabs[$globalTabsLength - 1 - $k]['position']);
                    $globalTabsInversePosition[] = $tab;
                }

                foreach ($globalTabsInversePosition as $tab) {
                    $this->productTabs->insert([
                        'product_id'    => $productId,
                        'global_tab_id' => $tab['id'],
                        'position'      => $tab['position'],
                        'enabled'       => $tab['enabled'],
                    ]);
                }
                if ($categories) {
                    $category = $categories[array_rand($categories)];
                    $this->categoryProducts->insert([
                        'category_id' => $category['category_id'],
                        'product_id'  => $productId,
                    ]);
                }

                if ($attributesOptions) {
                    $productAttributesOptionsPool = $attributesOptions;
                    shuffle($productAttributesOptionsPool);
                    $options           = [];
                    $firstIsSelectable = 0;
                    foreach ($productAttributesOptionsPool as $option) {
                        if (!$firstIsSelectable) {
                            $firstIsSelectable = $option['attribute_id'];
                        }
                        if (!isset($options[$option['attribute_id']]) || $firstIsSelectable == $option['attribute_id']) {
                            if (!isset($options[$option['attribute_id']])) {
                                $options[$option['attribute_id']] = [];
                            }
                            $options[$option['attribute_id']][] = $option;
                        }
                    }

                    foreach ($options as $attrId => $list) {
                        foreach ($list as $option) {
                            $this->attributeValuesSelect->insert([
                                'attribute_option_id' => $option['id'],
                                'product_id'          => $productId,
                                'attribute_id'        => $attrId,
                                'priceModifier'       => 0,
                                'priceModifierType'   => 'a',
                                'weightModifier'      => 0,
                                'weightModifierType'  => 'a',
                            ]);
                        }
                    }
                }

                $created++;
                $connection->commit();
            } catch (\Exception $exception) {
                $connection->rollBack();
                throw $exception;
            }
        }

        return $created;
    }

    /**
     * @return array
     */
    protected function getProductXData()
    {
        return [
            'name'             => 'XcartTools product #{X}',
            'briefDescription' => 'XcartTools Product #{X} Brief Description',
            'description'      => 'XcartTools Product #{X} Full Description',
            'sku'              => 'xtools-product-{X}',
        ];
    }

    /**
     * @return string
     */
    protected function generateUUIDv4()
    {
        return sprintf('%04x%04x-%04x-%04x-%04x-%04x%04x%04x',
            mt_rand(0, 0xffff), mt_rand(0, 0xffff),
            mt_rand(0, 0xffff),
            mt_rand(0, 0x0fff) | 0x4000,
            mt_rand(0, 0x3fff) | 0x8000,
            mt_rand(0, 0xffff), mt_rand(0, 0xffff), mt_rand(0, 0xffff)
        );
    }

    /**
     * @param string $string
     * @param string $id
     *
     * @return string|string[]
     */
    protected function prepareProductXName($string = '', $id = 'std')
    {
        return str_replace('{X}', $id, $string);
    }

    /**
     * @param int $min
     * @param int $max
     *
     * @return false|float
     */
    protected function getProductRandPrice($min = 10, $max = 100)
    {
        $minC = $min > 0 ? $min * 100 : 0;
        $maxC = $max > 0 ? $max * 100 : 0;
        $maxC = max($minC, $maxC);
        if ($minC == $maxC) {
            return round($minC / 100, 2);
        } else {
            return round(rand($minC, $maxC) / 100, 2);
        }
    }

    /**
     * @return DateTimeZone
     */
    protected function getTimeZone()
    {
        return new \DateTimeZone(date_default_timezone_get());
    }

    /**
     * @return int
     * @throws \Exception
     */
    protected function time()
    {
        $time = new \DateTime('now', $this->getTimeZone());

        return $time->getTimestamp();
    }
}