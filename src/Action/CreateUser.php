<?php
/**
 * Copyright (c) 2011-present Qualiteam software Ltd. All rights reserved.
 * See https://www.x-cart.com/license-agreement.html for license details.
 */

namespace XcartTools\Action;

use Exception;
use XcartTools\Repository\ProfileRoles;
use XcartTools\Repository\Profiles;

class CreateUser
{
    /**
     * @var Profiles
     */
    private $profiles;

    /**
     * @var ProfileRoles
     */
    private $profileRoles;

    public function __construct(
        Profiles $profiles,
        ProfileRoles $profileRoles
    ) {
        $this->profiles     = $profiles;
        $this->profileRoles = $profileRoles;
    }

    /**
     * @param string $login
     * @param string $password
     * @param bool   $isAdmin
     *
     * @throws Exception
     */
    public function execute($login, $password, $isAdmin = false, $remove = false)
    {
        $exist = $this->profiles->findBy('login', $login);
        if ($exist && !$remove) {
            throw new Exception('User ' . $login . ' already exists');
        }

        if ($remove) {
            if ($exist) {
                $this->profiles->remove($exist['profile_id']);
            } else {
                throw new Exception('User ' . $login . ' not found');
            }
        } else {
            $profileId = $this->profiles->insert([
                'login' => $login,
                'password' => $this->getPasswordHash($password),
                'status' => 'E',
                'language' => 'en',
                'access_level' => $isAdmin ? '100' : '0',
            ]);

            if ($isAdmin) {
                $this->profileRoles->insert([
                    'role_id' => 1,
                    'profile_id' => $profileId,
                ]);
            }
        }
    }

    /**
     * @param string $password
     *
     * @return string
     */
    protected function getPasswordHash($password)
    {
        return md5($password);
    }
}