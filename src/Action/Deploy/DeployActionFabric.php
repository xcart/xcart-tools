<?php
/**
 * Copyright (c) 2011-present Qualiteam software Ltd. All rights reserved.
 * See https://www.x-cart.com/license-agreement.html for license details.
 */

namespace XcartTools\Action\Deploy;

use Exception;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\Process\Process;
use XcartTools\Action\RebuildXcartViaInAppMarketplace;
use XcartTools\Action\RebuildXcartViaOldSystem;
use XcartTools\Action\RebuildXcartViaServiceTool;
use XcartTools\Context\ContextInterface;
use XcartTools\Domain\Provider\Config;
use XcartTools\Domain\Provider\Database;
use XcartTools\Utils\XcartPathLocator;
use XcartTools\Command\RedeployCommand;
use XcartTools\Domain\Provider\CoreInfo;

class DeployActionFabric
{
    const FROM_LATEST = 'latest';
    const FROM_REPO = 'repo';
    const FROM_EXTERNAL = 'external';

    protected const PROCESS_TIMEOUT = 300;

    /**
     * @var XcartPathLocator
     */
    protected $pathLocator;

    /**
     * @var SymfonyStyle
     */
    protected $io;

    /**
     * @var InputInterface
     */
    protected $input;

    /**
     * @var OutputInterface
     */
    protected $output;
    /**
     * @var string
     */
    protected $version;

    /**
     * @var Database
     */
//    protected $database;

    public function __construct(
        XcartPathLocator $pathLocator,
        InputInterface $input,
        OutputInterface $output

    ) {
        $this->pathLocator = $pathLocator;
        $this->io = new SymfonyStyle($input, $output);
        $this->input = $input;
        $this->output = $output;
    }

    /**
     * @param ContextInterface $context
     * @param array            $options
     *
     * @return string
     */
    public function execute(ContextInterface $context, array $options)
    {
        if (!$this->validateDBCredentials($options)) {
            $this->io->error("DB credentials are not valid. Please check. \nYou had entered:\nUser - "
                . ($options['dbUser'] ?: 'empty value')
                . "\nPassword -"
                . ($options['dbPass'] ?: 'empty value')
                . "\nDatabase name - "
                . ($options['dbName'] ?: 'empty value')
            );
            return '';
        }

        if (!$this->download($context, $options)) {
            $this->io->error('Aborted');
            return  '';
        }
        $this->unpack($context, $options);
        if (!$this->createDatabase($context, $options)) {
            $this->io->error('Aborted');
            return '';
        }
        if ($options['from'] != self::FROM_LATEST) {
            $db_imported = $this->importSqlDump($context, $options);
            $this->createConfig($context, $options);
            if ($db_imported) {
                $this->createAdmin($context, $options);
                $this->redeploy($context, $this->pathLocator, $options);
            } else {
                $this->io->warning('Admin user creating is skiped.');
            }
        }

        return 'X-Cart version ' . $this->getVersion() . ' is deployed. Please use install wizard to complete fresh X-Cart installation.';
    }

    /**
     * @param ContextInterface $context
     */
    protected function download(ContextInterface $context, array $options)
    {
        return true;
    }

    /**
     * @param array $options
     * @param ContextInterface $context
     */
    protected function unpack(ContextInterface $context, array $options)
    {
    }

    /**
     * @param array $options
     * @param ContextInterface $context
     */
    protected function createDatabase(ContextInterface $context, array $options)
    {
        $this->io->block('Database creating...');
        try {
            $context->exec([
                'mysql -u' . $options['dbUser'],
                '-p' . $options['dbPass'],
                '-e "CREATE DATABASE ' . $options['dbName'] . '"',
            ]);
        } catch (Exception $e) {
            $this->io->error($e->getMessage());
            return false;
        }

        return true;
    }

    /**
     * @param ContextInterface $context
     * @param array $options
     */
    protected function importSqlDump(ContextInterface $context, array $options)
    {
        $this->io->block('Importing database dump...');
        if (empty($options['dbDump']) || !file_exists($context->getCurrentDirectoryPath() . '/' . $options['dbDump'])) {
            $this->io->error('SQL dump not found.');
            return false;
        }

        return true;
    }

    /**
     * @param array $options
     * @return bool
     */
    private function validateDBCredentials(array $options)
    {
        return !(empty($options['dbName']) || empty($options['dbUser']) || empty($options['dbPass']));
    }

    /**
     * @param array $options
     * @param ContextInterface $context
     */
    protected function createConfig(ContextInterface $context, array $options)
    {
        $this->io->block('Creating config file...');
    }

    /**
     * @param array $options
     * @param ContextInterface $context
     */
    protected function createAdmin(ContextInterface $context, array $options)
    {
        $this->io->block('Creating admin user...');

    }

    /**
     * @param array $options
     * @param ContextInterface $context
     */
    protected function redeploy(ContextInterface $context, XcartPathLocator $pathLocator, array $options)
    {
        if ($pathLocator->isRemoteContext()) {
            $this->io->warning('Redeploy currently works only in local context');
            return false;
        }

        $this->io->block('Started redeploying X-Cart', 'INFO', 'fg=black;bg=cyan', ' ', true);
        try {
            $coreInfo = new CoreInfo($this->rootDir, $context);
            $this->clearPhpCache();
            $this->chmodStandardTool($pathLocator);

            if ($coreInfo->isInAppMarketplaceAvailable() && $this->isXcartToolAvailable($pathLocator)) {
                (new RebuildXcartViaInAppMarketplace($pathLocator))->execute($this->io);
            } else if ($coreInfo->isNewModuleSystemUsed()) {
                (new RebuildXcartViaServiceTool($pathLocator))->execute($this->io);
            } else {
                (new RebuildXcartViaOldSystem($pathLocator))->execute($this->io);
            }

            $this->io->success('Finished redeploying');

        } catch (Exception $e) {
            $this->io->error($e->getMessage());
            return false;
        }

        return true;
    }

    /**
     * @param array $options
     * @param ContextInterface $context
     * @return string
     */
    protected function getVersion()
    {
        if (!$this->version) {
            //TODO get X-Cart version from source
            $this->version = '<TODO>';
        }
        return $this->version;
    }

    protected function clearPhpCache()
    {
        if (function_exists('apc_clear_cache')) {
            apc_clear_cache();
        }

        if (function_exists('opcache_reset')) {
            opcache_reset();
        }
    }

    protected function chmodStandardTool(XcartPathLocator $pathLocator)
    {
        $path = $pathLocator->getXcartPath() . '/xc5';
        if (file_exists($path) && !\is_executable($path)) {
            $process = new Process(['chmod', '+x', './xc5'], $pathLocator->getXcartPath(), null, null, static::PROCESS_TIMEOUT);
            $process->mustRun();
        }
    }

    /**
     * @return bool
     * @throws \Symfony\Component\Process\Exception\LogicException
     */
    protected function isXcartToolAvailable(XcartPathLocator $pathLocator)
    {
        $path = $pathLocator->getXcartPath() . '/xc5';
        if (file_exists($path) && \is_executable($path)) {
            $process = new Process(['./xc5'], $pathLocator->getXcartPath(), null, null, 15);
            return $process->run() === 0;
        }
        return false;
    }
}