<?php
/**
 * Copyright (c) 2011-present Qualiteam software Ltd. All rights reserved.
 * See https://www.x-cart.com/license-agreement.html for license details.
 */

namespace XcartTools\Action\Deploy;

use XcartTools\Context\ContextInterface;

interface DeployActionInterface
{
    public function execute(ContextInterface $context, array $options);
}