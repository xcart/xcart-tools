<?php
/**
 * Copyright (c) 2011-present Qualiteam software Ltd. All rights reserved.
 * See https://www.x-cart.com/license-agreement.html for license details.
 */

namespace XcartTools\Action\Deploy;

use Exception;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\Filesystem\Filesystem;
use XcartTools\Action\CreateUser;
use XcartTools\Action\GetTables;
use XcartTools\Action\CreateDump;
use XcartTools\Context\ContextInterface;
use XcartTools\Context\LocalContext;
use XcartTools\Utils\ToolsPath;
use XcartTools\Utils\XcartPathLocator;
use XcartTools\Action\CreateConfigFile;
use XcartTools\Repository\ProfileRoles;
use XcartTools\Repository\Profiles;
use XcartTools\Domain\Provider\Database;
use XcartTools\Domain\Provider\Config;


class External extends DeployActionFabric
{
    /*
     * @var LocalContext
     */
    protected $localContext;

    /*
     * @var XcartPathLocator
     */
    protected $pathLocator;

    public function __construct(
        XcartPathLocator $pathLocator,
        InputInterface $input,
        OutputInterface $output
    ) {
        parent::__construct($pathLocator, $input, $output);
        $this->localContext = new LocalContext(new ToolsPath(), new Filesystem());
        $this->localPathLocator = new XcartPathLocator($this->localContext);
    }

    /**
     * @param ContextInterface $context
     */
    protected function download(ContextInterface $context, array $options)
    {
        parent::download($context, $options);
        $this->io->block('Downloading X-Cart from remote server');
        try {
            if (!$this->pathLocator->getXcartPath()) {
                $this->io->error('X-Cart not found');
                return false;
            }
            $context->exec('cd ' . $this->pathLocator->getXcartPath());
            $fileName = 'xc_dump.tgz';
            $tmpPath = $context->getTmpDir() . '/' . $fileName;
            $context->exec('cd ' . $this->pathLocator->getXcartPath() . '; tar -czf ' . $tmpPath . ' ' . implode(' ', $this->getFilesToArchive()));
            $context->download($tmpPath, 'xc_dump.tgz');
            $context->remove($tmpPath);
            $this->localContext->exec('tar -xzf ' . $fileName);
        } catch (Exception $e) {
            $this->io->error($e->getMessage());
            return false;
        }
        //$context->chdir($dst);

        return true;
    }

    /**
     * @param array $options
     * @param ContextInterface $context
     */
    protected function createDatabase(ContextInterface $context, array $options)
    {
        $this->io->block('Database creating...');
        try {
            $this->localContext->exec([
                'mysql -u' . $options['dbUser'],
                '-p' . $options['dbPass'],
                '-e "CREATE DATABASE ' . $options['dbName'] . '"',
            ]);
        } catch (Exception $e) {
            $this->io->error($e->getMessage());
            return false;
        }

        return true;
    }

    /**
     *
     * @param ContextInterface $context
     * @param array $options
     */
    protected function importSqlDump(ContextInterface $context, array $options)
    {
        $this->io->block('Downloading and importing database dump...');
        $config = new Config($this->pathLocator->getConfigDir(), $context);
        $command = new CreateDump(new GetTables());
        $createDumpOptions = [
            'user'      => $config->getDbUser(),
            'pass'      => $config->getDbPass(),
            'prefix'    => $config->getDbPrefix(),
            'mode'      => 'copy',
            'db'        => $config->getDbName()
        ];

        try {
            //ContextInterface $context, $path, $dbOptions
            $command->execute($context, 'db_dump.sql', $createDumpOptions);
        } catch (Exception $e) {
            $this->io->error($e->getMessage());
            return false;
        }
        try {
            $this->localContext->exec([
                'mysql -u' . $options['dbUser'],
                '-p' . $options['dbPass'],
                ' ' . $options['dbName'],
                ' < db_dump.sql',
            ]);
        } catch (Exception $e) {
            $this->io->error($e->getMessage());
            return false;
        }

        return true;
    }

    /**
     * @param array $options
     * @param ContextInterface $context
     */
    protected function createConfig(ContextInterface $context, array $options)
    {
        parent::createConfig($context, $options);
        $configOptions = [
            'dbSocket'  => isset($options['dbSocket']) ? $options['dbSocket'] : '',
            'dbPort'    => isset($options['dbPort']) ? $options['dbPort'] : '',
            'dbName'    => isset($options['dbName']) ? $options['dbName'] : '',
            'dbUser'    => isset($options['dbUser']) ? $options['dbUser'] : '',
            'dbPass'    => isset($options['dbPass']) ? $options['dbPass'] : '',
            'domain'    => isset($options['domain']) ? $options['domain'] : '',
            'webdir'    => isset($options['webDir']) ? $options['webDir'] : '',
            'authCode'  => isset($options['authCode']) ? $options['authCode'] : '',
            'secretKey' => isset($options['secretKey']) ? $options['secretKey'] : '',
            'devMode'   => (isset($options['devMode']) && $options['devMode']) ? 'On' : 'Off',
        ];
        try {
            $command = new CreateConfigFile($this->localPathLocator);
            $command->execute($this->localContext, $configOptions);
        } catch (Exception $e) {
            $this->io->error($e->getMessage());
            return false;
        }

        return true;
    }

    /**
     * @param array $options
     * @param ContextInterface $context
     */
    protected function createAdmin(ContextInterface $context, array $options)
    {
        parent::createAdmin($context, $options);
        $config = new Config($this->localPathLocator->getConfigDir(), $this->localContext);
        $db = new Database($config);
        $profiles = new Profiles($db);
        $profileRoles = new ProfileRoles($db);

        $command = new CreateUser($profiles, $profileRoles);
        try {
            $command->execute($options['adminUser'], $options['adminPass'], true);
        } catch (Exception $e) {
            $this->io->error($e->getMessage());
            return false;
        }

        return true;
    }

    private function getFilesToArchive()
    {
        return [
            '*.php',
            'classes/',
            'Includes',
            'etc',
            'files/service/',
            'lib',
            'modules_manager',
            'public',
            'service',
            'skins',
            'sql',
            'vendor',
            '.htaccess',
            'xc5'
        ];
    }

    /**
     * @param array $options
     * @param ContextInterface $context
     */
    protected function redeploy(ContextInterface $context, XcartPathLocator $pathLocator, array $options)
    {
        return parent::redeploy($this->localContext, $this->localPathLocator, $options);
    }
}