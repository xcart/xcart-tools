<?php
/**
 * Copyright (c) 2011-present Qualiteam software Ltd. All rights reserved.
 * See https://www.x-cart.com/license-agreement.html for license details.
 */

namespace XcartTools\Action\Deploy;

use Exception;
use Symfony\Component\Console\Helper\ProgressBar;
use XcartTools\Context\ContextInterface;
use XcartTools\Utils\XcartPathLocator;

class Latest extends DeployActionFabric
{
    const LATEST_URL = 'http://static.x-cart.com/xc5kit/latest.tgz';
    const OUTPUT_FILE_NAME = 'xcart_latest.tgz';

    /**
     * @param ContextInterface $context
     */
    protected function download(ContextInterface $context, array $options)
    {
        parent::download($context, $options);
        $this->io->block('Downloading the latest X-Cart version...');
        try {
            $context->exec('curl ' . self::LATEST_URL . ' --output ' . self::OUTPUT_FILE_NAME);
        } catch (Exception $e) {
            $this->io->error($e->getMessage());
            return false;
        }

        return true;
    }

    protected function unpack(ContextInterface $context, array $options)
    {
        parent::unpack($context, $options);
        $this->io->block('Unpacking X-Cart...');
        $context->exec('tar -xzf ' . self::OUTPUT_FILE_NAME);
        $context->exec('cp -r xcart/* . ');
        $context->exec('rm -rf xcart/ ' . self::OUTPUT_FILE_NAME);

    }
}