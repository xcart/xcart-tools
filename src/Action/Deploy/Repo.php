<?php
/**
 * Copyright (c) 2011-present Qualiteam software Ltd. All rights reserved.
 * See https://www.x-cart.com/license-agreement.html for license details.
 */

namespace XcartTools\Action\Deploy;

use Exception;
use Symfony\Component\Console\Helper\ProgressBar;
use XcartTools\Action\CreateUser;
use XcartTools\Context\ContextInterface;
use XcartTools\Utils\XcartPathLocator;
use XcartTools\Action\CreateConfigFile;
use XcartTools\Repository\ProfileRoles;
use XcartTools\Repository\Profiles;
use XcartTools\Domain\Provider\Database;
use XcartTools\Domain\Provider\Config;


class Repo extends DeployActionFabric
{
    /**
     * @param ContextInterface $context
     */
    protected function download(ContextInterface $context, array $options)
    {
        parent::download($context, $options);
        $this->io->block('Cloning repo...');
        $dst = $options['destinationDir'];
        if (empty($dst)) {
            preg_match('\/(.+)\.git', $options['repo'], $m);
            $dst = $m[1];
        }
        $context->chdir('..');
        try {
            $context->exec('git clone ' . $options['repo'] . ' ' . $dst);
        } catch (Exception $e) {
            $this->io->error($e->getMessage());
            return false;
        }
        $context->chdir($dst);

        return true;
    }

    /**
     * @param ContextInterface $context
     * @param array $options
     */
    protected function importSqlDump(ContextInterface $context, array $options)
    {
        if (parent::importSqlDump($context, $options)) {

            $options['dbDump'] = $this->prepareDumpFile($context, $options['dbDump']);
            try {
                $context->exec([
                    'mysql -u' . $options['dbUser'],
                    '-p' . $options['dbPass'],
                    ' ' . $options['dbName'],
                    ' < ' . $options['dbDump'],
                ]);
            } catch (Exception $e) {
                $this->io->error($e->getMessage());
                return false;
            }

            return true;
        }

        return false;
    }

    /**
     * @param array $options
     * @param ContextInterface $context
     */
    protected function createConfig(ContextInterface $context, array $options)
    {
        parent::createConfig($context, $options);
        $configOptions = [
            'dbSocket'  => isset($options['dbSocket']) ? $options['dbSocket'] : '',
            'dbPort'    => isset($options['dbPort']) ? $options['dbPort'] : '',
            'dbName'    => isset($options['dbName']) ? $options['dbName'] : '',
            'dbUser'    => isset($options['dbUser']) ? $options['dbUser'] : '',
            'dbPass'    => isset($options['dbPass']) ? $options['dbPass'] : '',
            'domain'    => isset($options['domain']) ? $options['domain'] : '',
            'webdir'    => isset($options['webDir']) ? $options['webDir'] : '',
            'authCode'  => isset($options['authCode']) ? $options['authCode'] : '',
            'secretKey' => isset($options['secretKey']) ? $options['secretKey'] : '',
            'devMode'   => (isset($options['devMode']) && $options['devMode']) ? 'On' : 'Off',
        ];
        try {
            $command = new CreateConfigFile($this->pathLocator);
            $command->execute($context, $configOptions);
        } catch (Exception $e) {
            $this->io->error($e->getMessage());
            return false;
        }

        return true;
    }

    /**
     * @param array $options
     * @param ContextInterface $context
     */
    protected function createAdmin(ContextInterface $context, array $options)
    {
        parent::createAdmin($context, $options);
        $config = new Config($this->pathLocator->getConfigDir(), $context);
        $db = new Database($config);
        $profiles = new Profiles($db);
        $profileRoles = new ProfileRoles($db);

        $command = new CreateUser($profiles, $profileRoles);
        try {
            $command->execute($options['adminUser'], $options['adminPass'], true);
        } catch (Exception $e) {
            $this->io->error($e->getMessage());
            return false;
        }

        return true;
    }

    private function prepareDumpFile(ContextInterface $context, $path)
    {
        preg_match('/\.([a-z._]+)/', $path, $match);
        $ext = $match[1];
       // var_dump($ext);die;
        $basename = basename($path);
        $fileName = preg_replace('/\.[a-z._]+/', '', $basename);
        switch ($ext) {
            case 'sql':
                return $path;

            case "gz":
            case "sql.gz":
                $context->exec('gunzip ' . $path);
                return str_replace($basename, $fileName . '.sql', $path);

            case "tgz":
            case "tar.gz":
                $_path = $context->exec('tar -tf ' . $path);
                $context->exec('tar -xzf ' . $path);
                return $_path;
        }

        return false;
    }
}