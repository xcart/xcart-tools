<?php
/**
 * Copyright (c) 2011-present Qualiteam software Ltd. All rights reserved.
 * See https://www.x-cart.com/license-agreement.html for license details.
 */

namespace XcartTools\Action;

use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use XcartTools\Context\ContextInterface;
use XcartTools\Utils\XcartPathLocator;
use XcartTools\Action\Deploy\DeployActionFabric;

class DeployXcart implements ActionInterface
{
    /**
     * @var XcartPathLocator
     */
    private $pathLocator;

    /**
     * @var DeployActionFabric
     */
    protected $deployAction;

    public function __construct(XcartPathLocator $pathLocator)
    {
        $this->pathLocator = $pathLocator;
    }

    /**
     * @param ContextInterface $context
     * @param array            $options
     *
     * @return string
     */
    public function execute(ContextInterface $context, $options, InputInterface $input, OutputInterface $output)
    {
        $actionClass = '\XcartTools\Action\Deploy\\'
            . str_replace(
                ' ',
                '',
                ucwords(preg_replace('/[^a-zA-Z0-9\x7f-\xff]++/', ' ', $options['from']))
            );

        $this->deployAction = new $actionClass($this->pathLocator, $input, $output);
        $result = $this->deployAction->execute($context, $options);

        return $result;
    }
}