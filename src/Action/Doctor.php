<?php
/**
 * Copyright (c) 2011-present Qualiteam software Ltd. All rights reserved.
 * See https://www.x-cart.com/license-agreement.html for license details.
 */

namespace XcartTools\Action;

use XcartTools\Context\ContextInterface;
use XcartTools\Utils\XcartPathLocator;

class Doctor implements ActionInterface
{
    /**
     * @var XcartPathLocator
     */
    private $pathLocator;

    public function __construct(XcartPathLocator $pathLocator)
    {
        $this->pathLocator = $pathLocator;
    }

    /**
     * @param ContextInterface $context
     * @param array            $options
     *
     * @return string
     */
    public function execute(ContextInterface $context, $options)
    {
        $actionClass = '\XcartTools\Action\Doctor\\'
            . str_replace(
                ' ',
                '',
                ucwords(preg_replace('/[^a-zA-Z0-9\x7f-\xff]++/', ' ', $options['mode']))
            );

        $action = new $actionClass($this->pathLocator);
        $result = $action->execute($context);

        return $result;
    }
}