<?php
/**
 * Copyright (c) 2011-present Qualiteam software Ltd. All rights reserved.
 * See https://www.x-cart.com/license-agreement.html for license details.
 */

namespace XcartTools\Action\Doctor;

use XcartTools\Context\ContextInterface;
use XcartTools\Context\RemoteOverSshContext;
use XcartTools\Utils\XcartPathLocator;

class ClearRebuild implements DoctorActionInterface
{
    /**
     * @var XcartPathLocator
     */
    private $pathLocator;

    public function __construct(XcartPathLocator $pathLocator)
    {
        $this->pathLocator = $pathLocator;
    }

    /**
     * @param ContextInterface $context
     * @param array            $options
     *
     * @return string
     */
    public function execute(ContextInterface $context)
    {
        foreach ($this->getFileMasksList($context) as $mask) {
            if ($context instanceof RemoteOverSshContext) {
                $context->remove($mask);
            } else {
                foreach (glob($mask, GLOB_MARK | GLOB_BRACE) as $file) {
                    $context->remove($file);
                }
            }
        }
        return 'All the temp files that are connected to crashed rebuild have been removed.';
    }

    private function getFileMasksList(ContextInterface $context)
    {
        $paths = [
            $this->pathLocator->getXcartVarPath() . '/datacache.*',
            $this->pathLocator->getXcartVarPath() . '/locale.*',
            $this->pathLocator->getXcartVarPath() . '/run.*',
            $this->pathLocator->getXcartVarPath() . '/tmp.*',
            $this->pathLocator->getScriptStateMask(),
            $this->pathLocator->getXcartFilesServicePath() . '/scenarioStorage.data'
        ];
        if ($context instanceof RemoteOverSshContext) {
            $paths[] = $this->pathLocator->getXcartVarPath() . '/.rebuild*';
            $paths[] = $this->pathLocator->getXcartVarPath() . '/.cacheInfo*';
        } else {
            $paths[] = $this->pathLocator->getXcartVarPath() . '/{,.}rebuild*';
            $paths[] = $this->pathLocator->getXcartVarPath() . '/{,.}cacheInfo.*';
        }
        return $paths;
    }
}