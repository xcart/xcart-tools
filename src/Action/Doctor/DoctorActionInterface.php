<?php
/**
 * Copyright (c) 2011-present Qualiteam software Ltd. All rights reserved.
 * See https://www.x-cart.com/license-agreement.html for license details.
 */

namespace XcartTools\Action\Doctor;

use XcartTools\Context\ContextInterface;

interface DoctorActionInterface
{
    public function execute(ContextInterface $context);
}