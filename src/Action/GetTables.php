<?php
/**
 * Copyright (c) 2011-present Qualiteam software Ltd. All rights reserved.
 * See https://www.x-cart.com/license-agreement.html for license details.
 */

namespace XcartTools\Action;

use DateTimeZone;
use XcartTools\Command\CreateDumpCommand;
use XcartTools\Context\ContextInterface;
use XcartTools\Domain\Provider\Database;
use XcartTools\Domain\Provider\ModuleInfo;
use XcartTools\Repository\Attributes;
use XcartTools\Repository\AttributesOptions;
use XcartTools\Repository\AttributeValuesSelect;
use XcartTools\Repository\Categories;
use XcartTools\Repository\CategoryProducts;
use XcartTools\Repository\GlobalProductTabs;
use XcartTools\Repository\Languages;
use XcartTools\Repository\Products;
use XcartTools\Repository\ProductTabs;
use XcartTools\Repository\ProductTranslations;
use function array_merge;
use function json_decode;

class GetTables implements ActionInterface
{
    public function __construct()
    {
    }

    /**
     * @param ContextInterface $context
     *
     * @param array            $dbOptions
     *
     * @return mixed|void
     */
    public function execute(ContextInterface $context, $dbOptions)
    {
        $user     = $dbOptions['user'];
        $pass     = $dbOptions['pass'];
        $db       = $dbOptions['db'];
        $port     = isset($dbOptions['port']) ? $dbOptions['port'] : 3306;
        $mode     = $dbOptions['mode'];
        $prefix   = $dbOptions['prefix'];

        $query              = <<<SQL
SHOW TABLES;
SQL;

        $result = $context->exec([
            'mysql',
            '--user="' . $user . '"',
            '--password="' . $pass . '"',
            '--port=' . $port,
            '--database=' . $db,
            '--execute="' . $query . '"',
        ]);

        return $result;
    }
}