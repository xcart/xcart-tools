<?php
/**
 * Copyright (c) 2011-present Qualiteam software Ltd. All rights reserved.
 * See https://www.x-cart.com/license-agreement.html for license details.
 */

namespace XcartTools\Action;

use Exception;
use http\Exception\BadMethodCallException;
use Symfony\Component\Process\Process;
use XcartTools\Context\ContextInterface;
use XcartTools\Utils\XcartPathLocator;

class Module implements ActionInterface
{
    protected const PROCESS_TIMEOUT = 300;

    /*
     * @var array
     */
    private $installedModules = [];

    /**§
     * @var XcartPathLocator
     */
    private $pathLocator;

    public function __construct(XcartPathLocator $pathLocator)
    {
        $this->pathLocator = $pathLocator;
    }

    /**
     * @param ContextInterface $context
     * @param array            $options
     *
     * @return string
     */
    public function execute(ContextInterface $context, $options)
    {
        if ($options['mode'] == 'new') {
            return $this->createModule($context, $options);
        } else if ($options['mode'] == 'install') {
            return $this->installModules($context, $options);
        }
    }


    /**
     * @param ContextInterface $context
     * @param $options
     * @return string
     */
    private function createModule(ContextInterface $context, $options)
    {
        $context->mkdir($this->pathLocator->getModulePath($options['authorId'], $options['moduleId']));
        $contentMainYaml = $this->buildMainYamlFile($options);
        $contentInstallYaml = $this->buildMainYamlFile($options);
        $context->write($this->pathLocator->getModuleMainYamlPath($options['authorId'], $options['moduleId']), $contentMainYaml);
        $context->write($this->pathLocator->getModuleInstallYamlPath($options['authorId'], $options['moduleId']), $contentInstallYaml);

        return $this->pathLocator->getModulePath($options['authorId'], $options['moduleId']);
    }

    private function installModules(ContextInterface $context, $options)
    {
        if (empty($options['source'])) {
            throw new Exception('[source] is empty');
        }
        if (is_file($this->pathLocator->getXcartPath() . '/' . $options['source'])) {
            $this->installedModules[] = $this->processFile($context, $options['source']);
        } elseif (is_dir($this->pathLocator->getXcartPath() . '/' . $options['source'])) {
            $this->installedModules = $this->installedModules + $this->processDir($context, $this->pathLocator->getXcartPath() . '/' . $options['source']);
        }

        if (!empty($this->installedModules)) {
            $this->redeploy();
            $this->enableInstalledModules();
        }
        return $this->getInstalledModulesString();
    }

    private function redeploy()
    {
        $process = new Process(['./xc5', 'marketplace:rebuild'], $this->pathLocator->getXcartPath(), null, null, static::PROCESS_TIMEOUT);
        $process->setTty(true);
        $process->start();

        while ($process->isRunning()) {
            echo $process->getIncrementalOutput();
        }
    }

    private function enableInstalledModules()
    {
        if (!empty($this->installedModules)) {
            $process = Process::fromShellCommandline('./xc5 marketplace:set-state -e ' .  $this->getInstalledModulesString(), $this->pathLocator->getXcartPath(), null, null, static::PROCESS_TIMEOUT);
            $process->setTty(true);
            $process->start();

            while ($process->isRunning()) {
                echo $process->getIncrementalOutput();
            }
        }
    }

    private function getInstalledModulesString()
    {
        $tmp = [];

        foreach ($this->installedModules as $module) {
            $tmp[] = $module['author'] . '-' . $module['id'];
        }
        return implode(',', $tmp);
    }

    /**
     * @param ContextInterface $context
     * @param $path
     * @return array
     */
    private function processDir(ContextInterface $context, $path)
    {
        $modulesParts = [];

        $files = $context->readDir($path);

        if (!$files) {
            throw new Exception('[source] directory is empty');
        }

        foreach ($files as $file) {
            $modulesParts[] = $this->processFile($context, $file);
        }
        return $modulesParts;
    }

    /**
     * @param ContextInterface $context
     * @param $fileName
     * @return array
     */
    private function processFile(ContextInterface $context, $fileName)
    {
        $context->exec('tar -xzf ' . $fileName);
        $moduleParts = [];
        if (preg_match('/^([a-zA-Z0-9]+)-([a-zA-Z0-9]+)\./', basename($fileName), $parts)) {
            $moduleParts['author'] = $parts[1];
            $moduleParts['id'] = $parts[2];
        }
        return $moduleParts;
    }
    /**
     * @param array $options
     *
     * @return string
     */
    protected function buildMainYamlFile($options)
    {
        $type = isset($options['moduleType']) ? $options['moduleType'] : 'common';
        $authorId   = isset($options['authorId']) ? $options['authorId'] : 'Qualiteam';
        $moduleName   = isset($options['moduleName']) ? $options['moduleName'] : 'Custom Module';
        $moduleDescription   = isset($options['moduleDescription']) ? $options['moduleDescription'] : 'Custom Module description';

        return <<<MAIN
version: 5.4.0.0
type: $type
authorName: $authorId
moduleName: '$moduleName'
description: '$moduleDescription'
minorRequiredCoreVersion: 0
dependsOn: {  }
incompatibleWith: {  }
skins: {  }
showSettingsForm: true
canDisable: true
MAIN;
    }

    /**
     * @param array $options
     *
     * @return string
     */
    protected function buildInstallYamlFile($options)
    {
        return <<<INSTALL
# vim: set ts=2 sw=2 sts=2 et:
#
# Fixtures
#
# Copyright (c) 2011-present Qualiteam software Ltd. All rights reserved.
# See https://www.x-cart.com/license-agreement.html for license details.

# Some exapmles
#XLite\Model\Config:
#  - name: exmpl_item
#    category: 'Author\Module'
#    type: 'XLite\View\FormField\Input\Checkbox\YesNo'
#    orderby: 100
#    value: true
#    translations:
#      - code: en
#        option_name: 'Some setting'

#XLite\Model\LanguageLabel:
#  - { name: "[exmpl] Some Label", translations: [{ code: en, label: "Some Label" }] }
INSTALL;
    }
}