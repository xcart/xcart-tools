<?php
/**
 * Copyright (c) 2011-present Qualiteam software Ltd. All rights reserved.
 * See https://www.x-cart.com/license-agreement.html for license details.
 */

namespace XcartTools\Action {

    use XcartTools\Context\ContextInterface;
    use XcartTools\Context\LocalContext;
    use XcartTools\Context\RemoteOverSshContext;
    use XcartTools\Domain\Module;
    use XcartTools\Domain\Package;
    use XcartTools\Domain\Provider\ModuleInfo;
    use XcartTools\Utils\XcartPathLocator;

    class PackModule implements ActionInterface
    {
        /**
         * @var ModuleInfo
         */
        private $moduleInfo;

        /**
         * @var Package
         */
        private $package;

        /**
         * @var XcartPathLocator
         */
        private $pathLocator;

        public function __construct(ModuleInfo $moduleInfo, Package $package, XcartPathLocator $pathLocator)
        {
            $this->moduleInfo  = $moduleInfo;
            $this->package     = $package;
            $this->pathLocator = $pathLocator;
        }

        /**
         * @param ContextInterface $context
         *
         * @param string           $moduleId
         * @param string           $outputDir
         *
         * @return mixed|void
         */
        public function execute(ContextInterface $context, $moduleId, $outputDir)
        {
            $this->prepareXcartConsts();
            $path = $this->packModule($moduleId);
            $newPath = $outputDir . \DIRECTORY_SEPARATOR . basename($path);

            if ($context instanceof RemoteOverSshContext) {
                $context->download($path, $newPath);
                $context->remove($path);
            } elseif ($context instanceof LocalContext) {
                $context->move($path, $newPath);
            }

            return $newPath;
        }

        /**
         * @param $moduleId
         *
         * @return string
         */
        protected function packModule($moduleId)
        {
            $module = new Module($this->moduleInfo->getModuleInfo($moduleId));

            return realpath($this->package->fromModule($module)->createPackage());
        }

        /**
         * Predefined consts to use with Module packer
         */
        protected function prepareXcartConsts()
        {
            // Short name
            define('LC_DS', \DIRECTORY_SEPARATOR);

            // Modes
            define('LC_IS_CLI_MODE', 'cli' === PHP_SAPI);

            // Common end-of-line
            define('LC_EOL', LC_IS_CLI_MODE ? "\n" : '<br />');

            // Timestamp of the application start
            define('LC_START_TIME', time());

            // Namespaces
            define('LC_NAMESPACE', 'XLite');
            define('LC_NAMESPACE_INCLUDES', 'Includes');
            define('LC_MODEL_NS', LC_NAMESPACE . '\Model');
            define('LC_MODEL_PROXY_NS', LC_MODEL_NS . '\Proxy');

            // Paths
            define('LC_DIR', realpath($this->pathLocator->getXcartPath()));
            define('LC_DIR_ROOT', rtrim(LC_DIR, LC_DS) . LC_DS);
            define('LC_DIR_CLASSES', LC_DIR_ROOT . 'classes' . LC_DS);
            define('LC_DIR_VAR', LC_DIR_ROOT . 'var' . LC_DS);
            define('LC_DIR_LIB', LC_DIR_ROOT . 'lib' . LC_DS);
            define('LC_DIR_SKINS', LC_DIR_ROOT . 'skins' . LC_DS);
            define('LC_DIR_IMAGES', LC_DIR_ROOT . 'images' . LC_DS);
            define('LC_DIR_FILES', LC_DIR_ROOT . 'files' . LC_DS);
            define('LC_DIR_CONFIG', LC_DIR_ROOT . 'etc' . LC_DS);
            define('LC_DIR_INCLUDES', LC_DIR_ROOT . LC_NAMESPACE_INCLUDES . LC_DS);
            define('LC_DIR_MODULES', LC_DIR_CLASSES . LC_NAMESPACE . LC_DS . 'Module' . LC_DS);
            define('LC_DIR_COMPILE', LC_DIR_VAR . 'run' . LC_DS);
            define('LC_DIR_CACHE_CLASSES', LC_DIR_COMPILE . 'classes' . LC_DS);
            define('LC_DIR_CACHE_SKINS', LC_DIR_COMPILE . 'skins' . LC_DS);
            define('LC_DIR_CACHE_MODULES', LC_DIR_CACHE_CLASSES . LC_NAMESPACE . LC_DS . 'Module' . LC_DS);
            define('LC_DIR_CACHE_MODEL', LC_DIR_CACHE_CLASSES . LC_NAMESPACE . LC_DS . 'Model' . LC_DS);
            define('LC_DIR_CACHE_PROXY', LC_DIR_CACHE_MODEL . 'Proxy' . LC_DS);
            define('LC_DIR_CACHE_RESOURCES', LC_DIR_VAR . 'resources' . LC_DS);
            define('LC_DIR_BACKUP', LC_DIR_VAR . 'backup' . LC_DS);
            define('LC_DIR_DATA', LC_DIR_VAR . 'data' . LC_DS);
            define('LC_DIR_TMP', LC_DIR_VAR . 'tmp' . LC_DS);
            define('LC_DIR_LOCALE', LC_DIR_VAR . 'locale');
            define('LC_DIR_DATACACHE', LC_DIR_VAR . 'datacache');
            define('LC_DIR_LOG', LC_DIR_VAR . 'log' . LC_DS);
            define('LC_DIR_CACHE_IMAGES', LC_DIR_VAR . 'images' . LC_DS);
            define('LC_DIR_SERVICE', LC_DIR_FILES . 'service' . LC_DS);
        }
    }
}

namespace {

    class XLite
    {
        const ADMIN_INTERFACE    = 'admin';
        const CUSTOMER_INTERFACE = 'customer';
        const CONSOLE_INTERFACE  = 'console';
        const MAIL_INTERFACE     = 'mail';
        const COMMON_INTERFACE   = 'common';
        const PDF_INTERFACE      = 'pdf';
    }
}