<?php
/**
 * Copyright (c) 2011-present Qualiteam software Ltd. All rights reserved.
 * See https://www.x-cart.com/license-agreement.html for license details.
 */

namespace XcartTools\Action;

use DateTimeZone;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\Process\Process;
use XcartTools\Command\CreateDumpCommand;
use XcartTools\Context\ContextInterface;
use XcartTools\Domain\Provider\Database;
use XcartTools\Domain\Provider\ModuleInfo;
use XcartTools\Repository\Attributes;
use XcartTools\Repository\AttributesOptions;
use XcartTools\Repository\AttributeValuesSelect;
use XcartTools\Repository\Categories;
use XcartTools\Repository\CategoryProducts;
use XcartTools\Repository\GlobalProductTabs;
use XcartTools\Repository\Languages;
use XcartTools\Repository\Products;
use XcartTools\Repository\ProductTabs;
use XcartTools\Repository\ProductTranslations;
use XcartTools\Utils\XcartPathLocator;
use function array_merge;
use function json_decode;

class RebuildXcartViaInAppMarketplace implements ActionInterface
{
    protected const PROCESS_TIMEOUT = 300;

    /**
     * @var XcartPathLocator
     */
    private $pathLocator;

    public function __construct(XcartPathLocator $pathLocator)
    {
        $this->pathLocator = $pathLocator;
    }

    /**
     * @param SymfonyStyle $io
     *
     * @return mixed|void
     */
    public function execute(SymfonyStyle $io)
    {
        $process = new Process(['./xc5', 'marketplace:rebuild'], $this->pathLocator->getXcartPath(), null, null, static::PROCESS_TIMEOUT);
        $process->setTty(true);
        $process->start();

        while ($process->isRunning()) {
            echo $process->getIncrementalOutput();
        }
    }
}