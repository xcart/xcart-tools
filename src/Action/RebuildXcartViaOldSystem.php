<?php
/**
 * Copyright (c) 2011-present Qualiteam software Ltd. All rights reserved.
 * See https://www.x-cart.com/license-agreement.html for license details.
 */

namespace XcartTools\Action;

use DateTimeZone;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\Process\Process;
use XcartTools\Command\CreateDumpCommand;
use XcartTools\Context\ContextInterface;
use XcartTools\Domain\Provider\Database;
use XcartTools\Domain\Provider\ModuleInfo;
use XcartTools\Repository\Attributes;
use XcartTools\Repository\AttributesOptions;
use XcartTools\Repository\AttributeValuesSelect;
use XcartTools\Repository\Categories;
use XcartTools\Repository\CategoryProducts;
use XcartTools\Repository\GlobalProductTabs;
use XcartTools\Repository\Languages;
use XcartTools\Repository\Products;
use XcartTools\Repository\ProductTabs;
use XcartTools\Repository\ProductTranslations;
use XcartTools\Utils\XcartPathLocator;
use function array_merge;
use function json_decode;

class RebuildXcartViaOldSystem implements ActionInterface
{
    protected const PROCESS_TIMEOUT = 300;

    /**
     * @var XcartPathLocator
     */
    private $pathLocator;

    public function __construct(XcartPathLocator $pathLocator)
    {
        $this->pathLocator = $pathLocator;
    }

    /**
     * @param SymfonyStyle     $io
     *
     * @return void
     */
    public function execute(SymfonyStyle $io)
    {
        $this->writeRebuildMark($io);
        $this->cleanupCacheIndicators();

        $io->writeln('Removed cache indicators');

        while ($this->isRebuildMarkPresent()) {
            $io->write($this->runXcart());
        }
    }

    protected function runXcart()
    {
        $process = new Process(['php', 'admin.php'], $this->pathLocator->getXcartPath(), null, null, static::PROCESS_TIMEOUT);
        $process->mustRun();

        return $process->getOutput();
    }

    protected function writeRebuildMark(SymfonyStyle $io)
    {
        @mkdir(dirname($this->pathLocator->getRebuildMarkFilepath()), 0777, true);
        $result = (bool) file_put_contents($this->pathLocator->getRebuildMarkFilepath(), $this->generateKey());
        if (!$result) {
            $io->error('Cannot write rebuild mark file at: ' . $this->pathLocator->getRebuildMarkFilepath());
            exit(1);
        }

        $io->writeln('Created rebuild mark file');

        return $result;
    }

    protected function isRebuildMarkPresent()
    {
        return (bool) file_exists($this->pathLocator->getRebuildMarkFilepath());
    }

    protected function cleanupCacheIndicators()
    {
        @array_map('unlink', glob($this->pathLocator->getCacheIndicatorMask()));
        @unlink($this->pathLocator->getRebuildStartedFilepath());
    }

    /**
     * Generate key
     *
     * @return string
     */
    public function generateKey()
    {
        return md5(microtime() . mt_rand(1, 1000));
    }
}