<?php
/**
 * Copyright (c) 2011-present Qualiteam software Ltd. All rights reserved.
 * See https://www.x-cart.com/license-agreement.html for license details.
 */

namespace XcartTools\Action;

use Exception;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\Process\Process;
use XcartTools\Patch\RebuildPhp;
use XcartTools\Utils\XcartPathLocator;

class RebuildXcartViaServiceTool implements ActionInterface
{
    protected const PROCESS_TIMEOUT = 300;
    protected const SHIM_PATH = TOOLS_ROOT . '/src/rebuild-shim.php';

    /**
     * @var XcartPathLocator
     */
    private $pathLocator;

    public function __construct(XcartPathLocator $pathLocator)
    {
        $this->pathLocator = $pathLocator;
    }

    /**
     * @param SymfonyStyle $io
     *
     * @return mixed|void
     * @throws \Throwable
     */
    public function execute(SymfonyStyle $io)
    {
        $xcartDir = $this->pathLocator->getXcartPath();

        if (!$this->copyShim($xcartDir)) {
            throw new Exception("Failed to copy shim to xcart folder");
        }

        $this->cleanupScriptStateStorage();
        $this->applyPatches();

        try {
            $process = new Process(['php', 'rebuild-shim.php', '--dir=' . $xcartDir], $xcartDir, null, null, static::PROCESS_TIMEOUT);
            $process->mustRun();
            $output = $process->getOutput();

            if (!$output || $process->getExitCode() > 0) {
                $io->error('Cannot access restore script');
                $io->writeln($output);
                throw new Exception($output);
            }

            $io->writeln('* Rebuild id: ' . $output);

            $this->chmodRebuildProcess();

            do {
                $result = $this->executeRebuildProcess($output);

                if ($result === 'done') {
                    // run once again to approve scenario script finished state
                    $this->executeRebuildProcess($output);
                } else {
                    $io->writeln('* ' . $result);
                }
            } while ($result !== 'done');

            $this->revertPatches();
            @unlink($xcartDir . '/rebuild-shim.php');
        } catch (\Throwable $e) {
            $this->revertPatches();
            @unlink($xcartDir . '/rebuild-shim.php');
            throw $e;
        }
    }

    protected function copyShim($destination)
    {
        if (\Phar::running()) {
            $phar = new \Phar(TOOLS_ROOT);
            $phar->extractTo($destination, 'src/rebuild-shim.php', true);
            return @rename($destination . '/src/rebuild-shim.php', $destination . '/rebuild-shim.php');
        } else {
            return @copy(static::SHIM_PATH, $destination . '/rebuild-shim.php');
        }
    }

    protected function cleanupScriptStateStorage()
    {
        @array_map('unlink', glob($this->pathLocator->getScriptStateMask()));
    }

    protected function chmodRebuildProcess()
    {
        if (!\is_executable($this->pathLocator->getXcartPath() . '/service/restore')) {
            $process = new Process(['chmod', '+x', 'service/restore'], $this->pathLocator->getXcartPath(), null, null, static::PROCESS_TIMEOUT);
            $process->mustRun();
        }
    }

    protected function executeRebuildProcess($id)
    {
        $process = new Process(['service/restore', '--action=rebuild', '--id=' . $id], $this->pathLocator->getXcartPath(), null, null, static::PROCESS_TIMEOUT);
        $process->mustRun();

        return $process->getOutput();
    }

    protected function applyPatches(): void
    {
        $rebuildPhpPath = $this->pathLocator->getXcartPath() . '/rebuild.php';
        if (\file_exists($rebuildPhpPath) && \is_writable($rebuildPhpPath)) {
            $patch = (new RebuildPhp())->getDiff();
            $process = new Process(['patch', './rebuild.php'], $this->pathLocator->getXcartPath(), null, $patch, static::PROCESS_TIMEOUT);
            $process->mustRun();
        }
    }

    protected function revertPatches(): void
    {
        $rebuildPhpPath = $this->pathLocator->getXcartPath() . '/rebuild.php';
        if (\file_exists($rebuildPhpPath) && \is_writable($rebuildPhpPath)) {
            $patch = (new RebuildPhp())->getDiff();
            $process = new Process(['patch', '--reverse', './rebuild.php'], $this->pathLocator->getXcartPath(), null, $patch, static::PROCESS_TIMEOUT);
            $process->mustRun();
        }
    }
}