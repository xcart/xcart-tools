<?php
/**
 * Copyright (c) 2011-present Qualiteam software Ltd. All rights reserved.
 * See https://www.x-cart.com/license-agreement.html for license details.
 */

namespace XcartTools\Action;

use DateTimeZone;
use XcartTools\Command\CreateDumpCommand;
use XcartTools\Context\ContextInterface;
use XcartTools\Domain\Provider\Database;
use XcartTools\Domain\Provider\ModuleInfo;
use XcartTools\Repository\Attributes;
use XcartTools\Repository\AttributesOptions;
use XcartTools\Repository\AttributeValuesSelect;
use XcartTools\Repository\Categories;
use XcartTools\Repository\CategoryProducts;
use XcartTools\Repository\GlobalProductTabs;
use XcartTools\Repository\Languages;
use XcartTools\Repository\Products;
use XcartTools\Repository\ProductTabs;
use XcartTools\Repository\ProductTranslations;
use function array_merge;
use function json_decode;

class ShowDatabases implements ActionInterface
{
    public function __construct()
    {
    }

    /**
     * @param ContextInterface $context
     *
     * @param array            $dbOptions
     *
     * @return mixed|void
     */
    public function execute(ContextInterface $context, $dbOptions)
    {
        $user     = $dbOptions['user'];
        $pass     = $dbOptions['pass'];
        $port     = isset($dbOptions['port']) ? $dbOptions['port'] : 3306;
        $mode     = $dbOptions['mode'];
        $prefix   = $dbOptions['prefix'];

        $ignoredTablesQuery = '';
        $ignoredTables = CreateDump::getIgnoredTablesList($mode);

        if ($ignoredTables) {
            $ignoredTables = array_map(function($table) use ($prefix) {
                return $prefix . $table;
            }, $ignoredTables);
            $ignoredTablesQuery = 'AND table_name NOT IN (\'' . implode("','", $ignoredTables) . '\')';
        }

        $query              = <<<SQL
SELECT table_schema, 
ROUND(SUM(data_length + index_length) / 1024 / 1024, 2) as size_mb
FROM information_schema.TABLES 
WHERE table_schema NOT LIKE 'information_schema' 
{$ignoredTablesQuery}
GROUP BY table_schema;
SQL;

        return $context->exec([
            'mysql',
            '--user="' . $user . '"',
            '--password="' . $pass . '"',
            '--port=' . $port,
            '--execute="' . $query . '"',
        ]);
    }
}