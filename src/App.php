<?php /** @noinspection PhpParamsInspection */
// vim: set ts=4 sw=4 sts=4 et:

/**
 * Copyright (c) 2011-present Qualiteam software Ltd. All rights reserved.
 * See https://www.x-cart.com/license-agreement.html for license details.
 */

namespace XcartTools;

use Symfony\Component\Console\ConsoleEvents;
use Symfony\Component\Console\Event\ConsoleCommandEvent;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\EventDispatcher\EventDispatcher;
use XcartTools\Command\ContextAwareInterface;
use XcartTools\Command\CreateDumpCommand;
use XcartTools\Context\ContextInterface;
use XcartTools\Context\LocalContext;
use XcartTools\Context\RemoteOverSshContext;
use Symfony\Bridge\ProxyManager\LazyProxy\Instantiator\RuntimeInstantiator;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\Console\Application;
use Symfony\Component\Console\Input\ArgvInput;
use Symfony\Component\Console\Output\ConsoleOutput;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Loader\YamlFileLoader;
use XcartTools\Command\CreateAdminUser;
use XcartTools\Command\CreateConfigCommand;
use XcartTools\Command\CreateProduct;
use XcartTools\Command\PackModuleCommand;
use XcartTools\Command\RedeployCommand;
use XcartTools\Command\UploadToStand;
use XcartTools\Command\ModuleCommand;
use XcartTools\Command\DoctorCommand;
use XcartTools\Command\DeployXcartCommand;
use XcartTools\Utils\XcartPathLocator;
use function set_error_handler;
use function set_exception_handler;
use function shell_exec;

class App
{
    use ContainerAwareTrait;

    const APP_NAME = "X-Cart Tools";
    const APP_VERSION = "1.3.17";

    /**
     * @var Application
     */
    protected $application;

    /**
     * App constructor.
     * @throws \Throwable
     */
    public function __construct()
    {
        $this->initializeContainer();
        $this->application = new Application(static::APP_NAME, static::APP_VERSION);
        $input = new ArgvInput();
        $output = new ConsoleOutput();
        $eventDispatcher = $this->container->get(EventDispatcher::class);
        $this->application->setDispatcher($eventDispatcher);
        $this->listenEvents($eventDispatcher);
        $this->addDefaultOptions();
        $this->registerCommands();
        $this->setExceptionHandler();
        $this->application->run($input, $output);
        $this->cleanup();
    }

    /**
     * @return void
     */
    protected function cleanup()
    {
        $context = $this->container->get(ContextInterface::class);
        if ($context instanceof RemoteOverSshContext) {
            $context->killMysqlTunnel();
        }
    }

    protected function setExceptionHandler()
    {
        $exceptionHandler = function ($e) {
            $this->cleanup();
        };

        $errorHandler = function ($errno, $errstr) {
            $this->cleanup();
        };

        $shutdownHandler = function () {
            $this->cleanup();
        };

        set_error_handler($exceptionHandler);
        set_exception_handler($errorHandler);
        register_shutdown_function($shutdownHandler);
    }

    /**
     * @throws \Exception
     */
    protected function initializeContainer()
    {
        $container = new ContainerBuilder();
        $container->setProxyInstantiator(new RuntimeInstantiator());
        $loader = new YamlFileLoader($container, new FileLocator(TOOLS_ROOT . '/config'));
        $loader->load('services.yaml');
        $container->compile();
        $this->setContainer($container);
    }

    protected function addDefaultOptions()
    {
        $this->application->getDefinition()->addOptions([
            new InputOption('ssh_string', 's', InputOption::VALUE_OPTIONAL, 'SSH user@host string'),
            new InputOption('ssh_port', 'P', InputOption::VALUE_OPTIONAL, 'SSH port', 22)
        ]);
    }

    protected function listenEvents(EventDispatcher $dispatcher)
    {
        $this->container->set(ContextInterface::class, $this->container->get(LocalContext::class));
        $dispatcher->addListener(ConsoleEvents::COMMAND, [$this, 'buildContext']);
    }

    /**
     * @param ConsoleCommandEvent $event
     */
    public function buildContext(ConsoleCommandEvent $event)
    {
        $input = $event->getInput();
        $output = $event->getOutput();
        $io = new SymfonyStyle($input, $output);

        if ($input->getOption('ssh_string')) {
            $host = $input->getOption('ssh_string');
            $port = $input->getOption('ssh_port');
            $this->container->set(ContextInterface::class, new RemoteOverSshContext($host, $port));
            $io->caution('Running in remote ssh context - ' . $host . PHP_EOL . 'All operations might be slower, please wait until finish');
        }

        /** @var ContextInterface $context */
        $context = $this->container->get(ContextInterface::class);

        /** @var XcartPathLocator $pathLocator */
        $pathLocator = $this->container->get(XcartPathLocator::class);
        $pathLocator->setContext($context);

        if ($event->getCommand() && $event->getCommand() instanceof ContextAwareInterface) {
            $event->getCommand()->setContext($context);
        }

        if (!$context->exists($pathLocator->getConfigFilepath())) {
            $io->warning('No X-Cart config is present');
        }

        if ($context->exists($pathLocator->getCustomerScriptPath())) {
            $io->section('X-Cart is detected - ' . $pathLocator->getXcartPath());
        }
    }

    /**
     * @return void
     * @throws \Exception
     */
    protected function registerCommands()
    {
        $this->application->add($this->container->get(RedeployCommand::class));
        $this->application->add($this->container->get(PackModuleCommand::class));
        $this->application->add($this->container->get(CreateConfigCommand::class));
        $this->application->add($this->container->get(CreateDumpCommand::class));
        $this->application->add($this->container->get(CreateAdminUser::class));
        $this->application->add($this->container->get(CreateProduct::class));
        $this->application->add($this->container->get(UploadToStand::class));
        $this->application->add($this->container->get(ModuleCommand::class));
        $this->application->add($this->container->get(DoctorCommand::class));
        $this->application->add($this->container->get(DeployXcartCommand::class));
    }
}
