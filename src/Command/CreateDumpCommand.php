<?php
// vim: set ts=4 sw=4 sts=4 et:

/**
 * Copyright (c) 2011-present Qualiteam software Ltd. All rights reserved.
 * See https://www.x-cart.com/license-agreement.html for license details.
 */

namespace XcartTools\Command;


use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use XcartTools\Action\CreateDump;
use XcartTools\Action\CreateUser;
use XcartTools\Action\ShowDatabases;
use XcartTools\Context\ContextInterface;
use XcartTools\Context\LocalContext;
use XcartTools\Context\RemoteOverSshContext;
use XcartTools\Domain\Provider\Config;
use XcartTools\Utils\ToolsPath;
use XcartTools\Utils\XcartPathLocator;
use function date;

class CreateDumpCommand extends Command implements ContextAwareInterface
{
    use InputHelperTrait;
    use SqlOutputHelperTrait;

    // the name of the command (the part after "bin/console")
    protected static $defaultName = 'dump';

    const MODE_FULL = 'full';
    const MODE_COPY = 'copy';

    /**
     * @var Config
     */
    protected $config;

    /**
     * @var array
     */
    private $dbOptions = [];

    /**
     * @var ShowDatabases
     */
    private $showDatabases;

    /**
     * @var XcartPathLocator
     */
    private $pathLocator;

    /**
     * @var CreateDump
     */
    private $action;

    /**
     * @var ContextInterface
     */
    private $context;

    /**
     * @var string
     */
    private $defaultDir;

    public function __construct(
        XcartPathLocator $pathLocator,
        CreateDump $action,
        ShowDatabases $showDatabases,
        ContextInterface $context,
        Config $config,
        $tmpDir,
        $name = null
    ) {
        parent::__construct($name);
        $this->pathLocator   = $pathLocator;
        $this->action        = $action;
        $this->showDatabases = $showDatabases;
        $this->context       = $context;
        $this->config        = $config;
        $this->defaultDir    = $tmpDir;
    }

    public function setContext(ContextInterface $context)
    {
        $this->context = $context;
    }

    protected function configure()
    {
        $this
            // the short description shown while running "php bin/console list"
            ->setDescription('Creates database dump. Can be run on remote host. Use -m param to set dump mode.')
            ->addArgument('outputDir', InputArgument::OPTIONAL, 'Directory to output created dump')
            ->addOption('mode', 'm', InputOption::VALUE_OPTIONAL, 'Dump mode, possible values: "full" (Full dump of database), "copy" (Secure copy of non-customer data & configuration)')
            ->addOption('db', null, InputOption::VALUE_OPTIONAL, 'Database to dump')
            ->addOption('port', null, InputOption::VALUE_OPTIONAL, 'MySQL connection port')
            ->addOption('user', null, InputOption::VALUE_REQUIRED, 'MySQL username')
            ->addOption('pass', null,InputOption::VALUE_REQUIRED, 'MySQL password')
            ->addOption('prefix', null, InputOption::VALUE_OPTIONAL, 'Database prefix')
            ->addOption('xc5_only', null, InputOption::VALUE_OPTIONAL, 'Only XC5 tables');
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     */
    protected function initialize(InputInterface $input, OutputInterface $output)
    {
        parent::initialize($input, $output);

        $this->dbOptions = $this->buildDbOptions($input, $output, $this->config);
    }

    /**
     * @param InputInterface  $input
     * @param OutputInterface $output
     *
     * @throws \Exception
     */
    protected function interact(InputInterface $input, OutputInterface $output)
    {
        parent::interact($input, $output);
        $io = new SymfonyStyle($input, $output);

        $io->title("Running Create DB Dump command");

        if (empty($this->dbOptions['user'])) {
            $this->dbOptions['user'] = $this->askIfEmpty($input, $output, 'user', 'Enter MySQL user', null);
        }

        if (empty($this->dbOptions['pass'])) {
            $this->dbOptions['pass'] = $this->askIfEmpty($input, $output, 'pass', 'Enter MySQL pass', null);
        }

        $this->dbOptions['mode'] = $this->askIfEmpty($input, $output, 'mode', 'Select dump mode between "full" and "copy"', 'copy');
        $this->dbOptions['xc5_only'] = $io->confirm("Do you want to ignore tables that are not connected to XC5 store?");

        $databases = $this->showDatabases->execute($this->context, $this->dbOptions);
        $this->renderSqlOutput($output, $databases, "Available databases");

        if (empty($this->dbOptions['db'])) {
            $this->dbOptions['db'] = $this->askIfEmpty($input, $output, 'db', 'Enter database name');
        } else {
            $io->text("DB to be dumped: " . $this->dbOptions['db']);
            $io->confirm("Proceed with dumping?");
        }
    }

    /**
     * @param InputInterface  $input
     *
     * @param OutputInterface $output
     *
     * @param Config          $config
     *
     * @return array
     * @throws \Exception
     */
    protected function buildDbOptions(InputInterface $input, OutputInterface $output, Config $config)
    {
        $options = [];
        try {
            $options['user']   = $config->getDbUser();
            $options['pass']   = $config->getDbPass();
            $options['prefix'] = $input->getOption('prefix') ?: $config->getDbPrefix();
            $options['db']     = $input->getOption('db') ?: $config->getDbName();
        } catch (\Throwable $e) {
            $options['user']   = $input->getOption('user');
            $options['pass']   = $input->getOption('pass');
            $options['prefix'] = $input->getOption('prefix');
            $options['db']     = $input->getOption('db');
        }

        $options['mode'] = $input->getOption('mode');

        return $options;
    }

    /**
     * @param InputInterface  $input
     * @param OutputInterface $output
     *
     * @return int|null|void
     * @throws \Exception
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $io = new SymfonyStyle($input, $output);

        $filename = $this->getDumpFilename();

        $outputPath = $input->getArgument('outputDir')
            ? $input->getArgument('outputDir') . '/' . $filename
            : $this->defaultDir . '/' . $filename;

        $path = $this->action->execute($this->context, $outputPath, $this->dbOptions);

        $io->block('Created database dump at: ' . $path, 'OK', 'fg=green', '');
    }

    /**
     * @return string
     */
    protected function getDumpFilename()
    {
        return $this->dbOptions['db'] . '_' . date("Ymd") . '.sql';
    }
}