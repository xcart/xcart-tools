<?php
// vim: set ts=4 sw=4 sts=4 et:

/**
 * Copyright (c) 2011-present Qualiteam software Ltd. All rights reserved.
 * See https://www.x-cart.com/license-agreement.html for license details.
 */

namespace XcartTools\Command;

use Exception;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use XcartTools\Action\CreateProducts;
use XcartTools\Context\ContextInterface;
use XcartTools\Context\LocalContext;
use XcartTools\Domain\Provider\Config;
use XcartTools\Utils\XcartPathLocator;

class CreateProduct extends Command implements ContextAwareInterface
{
    // the name of the command (the part after "bin/console")
    protected static $defaultName = 'product';

    /**
     * @var CreateProducts
     */
    private $action;

    /**
     * @var XcartPathLocator
     */
    private $pathLocator;

    /**
     * @var ContextInterface
     */
    private $context;

    /**
     * @var Config
     */
    private $config;

    public function __construct(
        XcartPathLocator $pathLocator,
        CreateProducts $action,
        LocalContext $context,
        Config $config,
        $name = null
    ) {
        parent::__construct($name);
        $this->pathLocator = $pathLocator;
        $this->action      = $action;
        $this->context     = $context;
        $this->config     = $config;
    }

    public function setContext(ContextInterface $context)
    {
        $this->context = $context;
    }

    protected function configure()
    {
        $this
            // the short description shown while running "php bin/console list"
            ->setDescription('Generates product entites into X-Cart DB')
            ->addOption('amount', 'a', InputOption::VALUE_OPTIONAL, 'Amount');
    }

    /**
     * @param InputInterface  $input
     * @param OutputInterface $output
     *
     * @throws Exception
     */
    protected function interact(InputInterface $input, OutputInterface $output)
    {
        parent::interact($input, $output);

        if (!$this->config->hasDbConfiguration()) {
            throw new Exception('Cannot setup db connection, check your config');
        }

        $this->askIfEmpty($input, $output, 'amount', 'Enter desired amount', '1');
    }

    protected function askIfEmpty(InputInterface $input, OutputInterface $output, $property, $question, $default = null)
    {
        $io = new SymfonyStyle($input, $output);

        if (!$input->getOption($property)) {
            $answer = $io->ask($question, $default);
            $input->setOption($property, $answer);
        }
    }

    /**
     * @param InputInterface  $input
     * @param OutputInterface $output
     *
     * @return int|null|void
     * @throws Exception
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $io = new SymfonyStyle($input, $output);

        $qty = $input->getOption('amount');

        try {
            $created = $this->action->execute($qty);
            $io->block('Created ' . $created . ' products', 'OK', 'fg=green', '');
        } catch (\Throwable $e) {
            $io->warning($e->getMessage());
        }
    }
}