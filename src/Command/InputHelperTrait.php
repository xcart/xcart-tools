<?php
// vim: set ts=4 sw=4 sts=4 et:

/**
 * Copyright (c) 2011-present Qualiteam software Ltd. All rights reserved.
 * See https://www.x-cart.com/license-agreement.html for license details.
 */

namespace XcartTools\Command;

use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

trait InputHelperTrait
{
    protected function askIfEmpty(InputInterface $input, OutputInterface $output, $property, $question, $default = null)
    {
        $io = new SymfonyStyle($input, $output);

        if (!$input->getOption($property)) {
            $answer = $io->ask($question, $default);
            $input->setOption($property, $answer);
        }

        return $input->getOption($property);
    }
}