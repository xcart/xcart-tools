<?php
// vim: set ts=4 sw=4 sts=4 et:

/**
 * Copyright (c) 2011-present Qualiteam software Ltd. All rights reserved.
 * See https://www.x-cart.com/license-agreement.html for license details.
 */

namespace XcartTools\Command;

use Exception;
use XcartTools\Action\PackModule;
use XcartTools\Context\ContextInterface;
use XcartTools\Utils\ToolsPath;
use function basename;
use function file_exists;
use function file_put_contents;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Process\Process;
use XcartTools\Domain\Module;
use XcartTools\Domain\Provider\ModuleInfo;
use XcartTools\Domain\Package;
use XcartTools\Utils\XcartPathLocator;

class PackModuleCommand extends Command implements ContextAwareInterface
{
    // the name of the command (the part after "bin/console")
    protected static $defaultName = 'pack';

    /**
     * @var XcartPathLocator
     */
    private $pathLocator;

    /**
     * @var ContextInterface
     */
    private $context;

    /**
     * @var PackModule
     */
    private $action;
    /**
     * @var ToolsPath
     */
    private $toolsPath;

    public function __construct(
        ContextInterface $context,
        XcartPathLocator $pathLocator,
        PackModule $action,
        ToolsPath $toolsPath,
        $name = null
    ) {
        parent::__construct($name);
        $this->pathLocator = $pathLocator;
        $this->context     = $context;
        $this->action      = $action;
        $this->toolsPath = $toolsPath;
    }

    public function setContext(ContextInterface $context)
    {
        $this->context = $context;
    }

    protected function configure()
    {
        $this
            // the short description shown while running "php bin/console list"
            ->setDescription('Creates module package by specified AuthorID-ModuleId name')
            ->addArgument('moduleId', InputArgument::REQUIRED, 'Module ID in Author-Name format')
            ->addArgument('outputDir', InputArgument::OPTIONAL, 'Directory to store created package');
    }

    /**
     * @param InputInterface  $input
     * @param OutputInterface $output
     *
     * @return int|null|void
     * @throws Exception
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $io = new SymfonyStyle($input, $output);

        if ($this->pathLocator->isRemoteContext()) {
            throw new Exception('This command currently works only in local context');
        }

        $moduleId  = $input->getArgument('moduleId');
        $io->writeln('...Creating pack for "' . $moduleId . '" module');

        $outputDir = $input->getArgument('outputDir')
            ? $input->getArgument('outputDir')
            : $this->toolsPath->getTmpDir();

        $path = $this->action->execute($this->context, $moduleId, $outputDir);

        $io->block('Pack is stored at: ' . $path, 'OK', 'fg=green', '');
    }
}