<?php
// vim: set ts=4 sw=4 sts=4 et:

/**
 * Copyright (c) 2011-present Qualiteam software Ltd. All rights reserved.
 * See https://www.x-cart.com/license-agreement.html for license details.
 */

namespace XcartTools\Command;

use Exception;
use Symfony\Component\Process\Exception\ProcessFailedException;
use XcartTools\Action\RebuildXcartViaInAppMarketplace;
use XcartTools\Action\RebuildXcartViaOldSystem;
use XcartTools\Action\RebuildXcartViaServiceTool;
use function file_exists;
use function file_put_contents;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\Process\Process;
use XcartTools\Domain\Provider\CoreInfo;
use XcartTools\Utils\XcartPathLocator;

class RedeployCommand extends Command
{
    // the name of the command (the part after "bin/console")
    protected static $defaultName = 'redeploy';

    protected const PROCESS_TIMEOUT = 300;

    /**
     * @var XcartPathLocator
     */
    private $pathLocator;

    /**
     * @var CoreInfo
     */
    private $coreInfo;

    public function __construct(XcartPathLocator $pathLocator, CoreInfo $coreInfo, $name = null)
    {
        parent::__construct($name);
        $this->pathLocator = $pathLocator;
        $this->coreInfo = $coreInfo;
    }

    protected function configure()
    {
        $this
            // the short description shown while running "php bin/console list"
            ->setDescription('Recreates X-Cart code cache and applies migrations\hooks')
        ;
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     *
     * @return int|null|void
     * @throws \Exception
     * @throws \Symfony\Component\Process\Exception\LogicException
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $io = new SymfonyStyle($input, $output);

        if ($this->pathLocator->isRemoteContext()) {
            throw new Exception('This command currently works only in local context');
        }

        $io->block('Started redeploying X-Cart', 'INFO', 'fg=black;bg=cyan', ' ', true);

        $this->clearPhpCache();

        $this->chmodStandardTool();

        if ($this->coreInfo->isInAppMarketplaceAvailable() && $this->isXcartToolAvailable()) {
            (new RebuildXcartViaInAppMarketplace($this->pathLocator))->execute($io);
        } else if ($this->coreInfo->isNewModuleSystemUsed()) {
            (new RebuildXcartViaServiceTool($this->pathLocator))->execute($io);
        } else {
            (new RebuildXcartViaOldSystem($this->pathLocator))->execute($io);
        }

        $io->success('Finished redeploying');
    }

    protected function chmodStandardTool()
    {
        $path = $this->pathLocator->getXcartPath() . '/xc5';
        if (file_exists($path) && !\is_executable($path)) {
            $process = new Process(['chmod', '+x', './xc5'], $this->pathLocator->getXcartPath(), null, null, static::PROCESS_TIMEOUT);
            $process->mustRun();
        }
    }

    /**
     * @return bool
     * @throws \Symfony\Component\Process\Exception\LogicException
     */
    protected function isXcartToolAvailable()
    {
        $path = $this->pathLocator->getXcartPath() . '/xc5';
        if (file_exists($path) && \is_executable($path)) {
            $process = new Process(['./xc5'], $this->rootDir, null, null, 15);
            return $process->run() === 0;
        }
        return false;
    }

    protected function clearPhpCache()
    {
        if (function_exists('apc_clear_cache')) {
            apc_clear_cache();
        }

        if (function_exists('opcache_reset')) {
            opcache_reset();
        }
    }
}