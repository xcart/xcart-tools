<?php
// vim: set ts=4 sw=4 sts=4 et:

/**
 * Copyright (c) 2011-present Qualiteam software Ltd. All rights reserved.
 * See https://www.x-cart.com/license-agreement.html for license details.
 */

namespace XcartTools\Command;

use Symfony\Component\Console\Helper\Table;
use Symfony\Component\Console\Output\OutputInterface;

trait SqlOutputHelperTrait
{
     protected function renderSqlOutput(OutputInterface $output, $queryResult, $title = null)
     {
         $rows = explode("\n", $queryResult);
         $rows = array_map(function($row) {
             return explode("\t", $row);
         }, $rows);
         $headers = array_shift($rows);

         $table = new Table($output);
         $table->setHeaderTitle($title);
         $table->setHeaders($headers);
         $table->setRows($rows);
         $table->render();
     }

     protected function sqlColumnToArray($queryResult)
     {
         $rows = explode("\n", $queryResult);
         $header = array_shift($rows);
         return $rows;
     }
}