<?php
// vim: set ts=4 sw=4 sts=4 et:

/**
 * Copyright (c) 2011-present Qualiteam software Ltd. All rights reserved.
 * See https://www.x-cart.com/license-agreement.html for license details.
 */

namespace XcartTools\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use XcartTools\Utils\StandSSHClient;
use XcartTools\Utils\StandSSHClientFactory;

class UploadToStand extends Command
{
    // the name of the command (the part after "bin/console")
    protected static $defaultName = 'standUpload';

    /**
     * @var StandSSHClientFactory
     */
    private $clientFactory;

    public function __construct(StandSSHClientFactory $clientFactory, $name = null)
    {
        parent::__construct($name);

        $this->clientFactory = $clientFactory;
    }

    protected function configure()
    {
        $this->setDescription('[BETA] Uploads X-Cart project to stand. Not for production usage')
            ->setHelp('If deploy is failed check that ssh-agent have required identity. ' . PHP_EOL
                . '    ssh-add -l list identities.' . PHP_EOL
                . '    ssh-add ~/.ssh/<your_key> - add identity to agent')
            ->addArgument('standname', InputArgument::REQUIRED, 'Stand name')
            ->addArgument('repoName', InputArgument::REQUIRED, 'Repository name')
            ->addOption('username', 'u',InputOption::VALUE_REQUIRED, 'Username', 'ubuntu')
            ->addOption('port', 'p',InputOption::VALUE_REQUIRED, 'Stand port', 22)
            ->addOption('dbDump', 'd',InputOption::VALUE_REQUIRED, 'Database dump')
            ->addOption('branch', 'b',InputOption::VALUE_REQUIRED, 'Repository branch', 'master')
            ->addOption('remoteDir', 'r',InputOption::VALUE_REQUIRED, 'Remote dir', 'deploy')
            ->addOption('initializeVendor', 'i',InputOption::VALUE_NONE, 'Composer install required')
            ->addOption('tablePrefix', 't',InputOption::VALUE_REQUIRED, 'Table prefix', 'xc_')
        ;
    }

    /**
     * @param InputInterface  $input
     * @param OutputInterface $output
     *
     * @return int|null|void
     * @throws \Exception
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $io = new SymfonyStyle($input, $output);

        $io->section('Connection');
        $io->writeln('Connecting...');

        $client = $this->tradeInputForClient($input);
        $client->testConnection();

        $io->success('Connected');

        $filesDirectory = __DIR__ . '/UploadToStand/';
        $client->executeCommand('mkdir -p deploy_data');
        $client->uploadFile($filesDirectory . '/robots.txt', 'deploy_data/robots.txt');
        $client->executeCommand('/bin/cp -rf ./deploy_data/robots.txt /var/www/html/robots.txt');

        $io->success('robots.txt copied');

        $io->section('Creating database');
        $databaseName = 'xc_db';
        $client->executeMySQLCommand("CREATE DATABASE IF NOT EXISTS $databaseName");

        $mysqlUser = 'xc_user';
        /** @noinspection CryptographicallySecureRandomnessInspection */
        /** @noinspection PhpComposerExtensionStubsInspection */
        $mysqlPassword = bin2hex(openssl_random_pseudo_bytes(5));
        $io->write($client->executeMySQLCommand("GRANT ALL PRIVILEGES ON $databaseName.* TO '$mysqlUser'@'localhost' IDENTIFIED BY '$mysqlPassword'"));

        if ($dbDumpPath = $input->getOption('dbDump')) {
            $remoteDumpPath = 'deploy_data/dump.sql';
            if (!$client->fileExists($remoteDumpPath)) {
                $io->section('Uploading database dump');
                $client->uploadFile($dbDumpPath, $remoteDumpPath);
                $client->executeCommand("mysql $databaseName < ./deploy_data/dump.sql");
            } else {
                $io->note('Dump already exists');
            }
        }
        $io->success('Database created');

        $repoName = $input->getArgument('repoName');
        $repoUri = "git@bitbucket.org:xcart/$repoName.git";
        $repoBranch = $input->getOption('branch');
        $remoteDir = ($dir = $input->getOption('remoteDir'))
            ? '/' . ltrim($dir, '/')
            : '';

        $io->section('Cloning repository');
        $remoteRealPath = '/var/www/html/' . ltrim($remoteDir, '/');

        $client->executeCommand('ssh-keygen -R bitbucket.org');
        $client->executeCommand('ssh-keyscan -H bitbucket.org >> ~/.ssh/known_hosts');
        $io->write($client->executeCommand("git clone $repoUri $remoteRealPath; cd $remoteRealPath; git checkout $repoBranch"));

        $io->success('Repository cloned');

        if ($input->getOption('initializeVendor')) {
            $io->section('Installing dependencies from composer.json');
            $client->uploadFile($filesDirectory . '/install_composer.sh', 'deploy_data/install_composer.sh');
            $client->executeCommand('chmod +x ./deploy_data/install_composer.sh; ./deploy_data/install_composer.sh');
            $io->success('Installed');
        }

        $io->section('Uploading config');
        $standClientHost = $input->getArgument('standname') . '.x-cart.biz';
        $configPath = $this->prepareConfig($filesDirectory . '/config_template.php.template', [
            'MYSQL_DATABASE_NAME' => $databaseName,
            'MYSQL_USER' => $mysqlUser,
            'MYSQL_PASSWORD' => $mysqlPassword,
            'TABLE_PREFIX'   => $input->getOption('tablePrefix'),
            'HTTP_HOST'   => $standClientHost,
            'WEB_DIR'   => $remoteDir
        ]);
        $client->uploadFile($configPath, $remoteRealPath . '/etc/config.php');

        $client->executeCommand("/bin/cp -rf ./deploy_data/robots.txt $remoteRealPath/robots.txt");

        $io->success('Finished');
        $standRoot = $standClientHost;
        $standRoot .= $remoteDir;
        $io->writeln("http://$standRoot/admin.php");
    }

    protected function prepareConfig($templatePath, $replace)
    {
        $content = file_get_contents($templatePath);

        foreach ($replace as $replaceName => $value) {
            $content = str_replace("%$replaceName%", $value, $content);
        }
        $path = $templatePath . '.replaced';
        file_put_contents($templatePath . '.replaced', $content);

        return $path;
    }

    /**
     * @param InputInterface $input
     *
     * @return StandSSHClient
     */
    protected function tradeInputForClient(InputInterface $input)
    {
        $standname = $input->getArgument('standname');
        $host = $standname . '.xcart-service.com';

        return $this->clientFactory->create(
            $host,
            $input->getOption('port'),
            $input->getOption('username')
        );
    }
}
