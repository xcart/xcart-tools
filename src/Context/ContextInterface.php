<?php
// vim: set ts=4 sw=4 sts=4 et:

/**
 * Copyright (c) 2011-present Qualiteam software Ltd. All rights reserved.
 * See https://www.x-cart.com/license-agreement.html for license details.
 */
namespace XcartTools\Context;

interface ContextInterface
{
    public function start();

    public function exec($cmd);

    public function finish();

    public function read($path);

    public function write($path, $content);

    public function remove($path);

    public function mkdir($path, $mode = 0777);

    public function exists($path);

    public function readable($path);

    public function getTmpDir();

    public function getCurrentDirectoryName();

    public function getCurrentDirectoryPath();

    public function chdir($path);

    public function readDir($path, $fileMask = '*');
}