<?php
// vim: set ts=4 sw=4 sts=4 et:

/**
 * Copyright (c) 2011-present Qualiteam software Ltd. All rights reserved.
 * See https://www.x-cart.com/license-agreement.html for license details.
 */
namespace XcartTools\Context;

use Exception;
use Symfony\Component\Process\Process;
use XcartTools\Utils\ToolsPath;
use function getcwd;
use Symfony\Component\Filesystem\Filesystem;

class LocalContext implements ContextInterface
{
    protected const PROCESS_TIMEOUT = 300;

    /**
     * @var ToolsPath
     */
    private $toolsPath;

    /**
     * @var ToolsPath
     */
    private $filesystem;

    public function __construct(ToolsPath $toolsPath, Filesystem $filesystem)
    {
        $this->toolsPath = $toolsPath;
        $this->filesystem = $filesystem;
    }

    public function start()
    {
    }

    /**
     * @param array|string $cmd
     * @param string       $pwd
     *
     * @return string
     * @throws Exception
     */
    public function exec($cmd, $pwd = null)
    {
        if (is_array($cmd)) {
            $cmd = implode(' ', $cmd);
        }

        $process = Process::fromShellCommandline($cmd, $pwd, null, null, static::PROCESS_TIMEOUT);
        $process->mustRun();
        $output = $process->getOutput();

        if (!$process->isSuccessful()) {
            throw new Exception('Error exit code from: ' . $cmd);
        }

        return trim($output);
    }

    public function finish()
    {
    }

    public function exists($path)
    {
        return (bool) file_exists($path);
    }

    public function read($path)
    {
        return file_get_contents($path);
    }

    public function write($path, $content)
    {
        return file_put_contents($path, $content);
    }

    /**
     * @param $path
     */
    public function remove($path)
    {
        $this->filesystem->remove($path);
    }

    /**
     * @param string $oldPath
     * @param string $newPath
     *
     * @return bool
     */
    public function move($oldPath, $newPath)
    {
        return rename($oldPath, $newPath);
    }

    /**
     * @param string $path
     * @param int    $mode
     *
     * @return bool
     */
    public function mkdir($path, $mode = 0777)
    {
        return @mkdir($path, $mode, true);
    }

    /**
     * @return string
     */
    public function getTmpDir()
    {
        return $this->toolsPath->getTmpDir();
    }

    /**
     * @param $path
     *
     * @return bool
     */
    public function readable($path)
    {
        return \is_readable($path);
    }

    /**
     * @return string
     */
    public function getCurrentDirectoryName()
    {
        return $this->exec('basename "$PWD"');
    }

    /**
     * @return string
     */
    public function getCurrentDirectoryPath()
    {
        return getcwd();
    }

    /**
     * @param string $path
     * @return bool
     */
    public function chdir($path)
    {
        return chdir($path);
    }


    /**
     * @param $path
     * @param string $fileMask
     * @return array|false
     */
    public function readDir($path, $fileMask = '*')
    {
        $files = glob("$path/$fileMask", GLOB_BRACE);
        return $files;
    }
}