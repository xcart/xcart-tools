<?php
// vim: set ts=4 sw=4 sts=4 et:

/**
 * Copyright (c) 2011-present Qualiteam software Ltd. All rights reserved.
 * See https://www.x-cart.com/license-agreement.html for license details.
 */
namespace XcartTools\Context;

use phpseclib\Net\SFTP;
use phpseclib\Net\SSH2;
use phpseclib\System\SSH\Agent;
use function preg_match;
use function shell_exec;
use function trim;

class RemoteOverSshContext implements ContextInterface
{
    const TUNNEL_MYSQL_PORT = 3307;

    /**
     * @var SSH2
     */
    protected $ssh;

    /**
     * @var SFTP
     */
    protected $sftp;

    /**
     * @var string
     */
    private $host;

    /**
     * @var int
     */
    private $port;

    /**
     * @var string
     */
    private $username;

    /**
     * @var string
     */
    private $cwd;

    /**
     * @var string
     */
    private $sshControlSocket;

    public function __construct($sshString, $port = 22)
    {
        $matches = null;

        if (preg_match("/^([A-Za-z0-9_-]*)@([A-Za-z0-9_.-]*):?([A-Za-z0-9_~\-.\/]*)*?$/", $sshString, $matches)) {
            $this->username = $matches[1];
            $this->host = $matches[2];
            $this->cwd = isset($matches[3]) ? $matches[3] : null;
        }

        $this->port = $port;
    }

    public function start()
    {
    }

    public function exec($cmd, $pwd = null, $callback = null)
    {
        if (is_array($cmd)) {
            $cmd = implode(' ', $cmd);
        }

        // TODO: Fix pwd
        $cmd = 'cd ' . $this->cwd . '; ' . $cmd;

        return trim($this->ssh()->exec($cmd, $callback));
    }

    public function execMysql($cmd)
    {
        return $this->exec("mysql --execute=\"$cmd\"");
    }

    public function finish()
    {
        $this->ssh()->disconnect();
        $this->sftp()->disconnect();
    }

    public function read($path)
    {
        return $this->sftp()->get($path, false);
    }

    public function write($path, $content)
    {
        return $this->sftp()->put($path, $content, SFTP::SOURCE_STRING);
    }

    public function mkdir($path, $mode = 755)
    {
        return $this->sftp()->mkdir($path, $mode, true);
    }

    public function remove($path)
    {
        return $this->ssh()->exec('rm -rf ' . $path);
    }

    public function exists($path)
    {
        return $this->sftp()->file_exists($path);
    }

    /**
     * @param $path
     *
     * @return bool
     */
    public function readable($path)
    {
        return $this->sftp()->is_readable($path);
    }

    public function upload($localPath, $remotePath)
    {
        return $this->sftp()->put($remotePath, $localPath, SFTP::SOURCE_LOCAL_FILE);
    }

    public function download($remotePath, $localPath)
    {
        return $this->sftp()->get($remotePath, $localPath);
    }

    /**
     * @param $remotePort
     */
    public function setUpMysqlTunnel($remotePort)
    {
        if (!$this->sshControlSocket) {
            $this->sshControlSocket = '/tmp/.ssh-mysql-tunnel.sock';
            $localPort = static::TUNNEL_MYSQL_PORT;
            $cmd = [
                "ssh",
                "-p{$this->port}",
                "-o StrictHostKeyChecking=no",
                "-M",
                "-S {$this->sshControlSocket}",
                "-fNg",
                "-L {$localPort}:127.0.0.1:{$remotePort}",
                "{$this->username}@{$this->host}"
            ];
            $cmd = implode(" ", $cmd);
            shell_exec($cmd);
        }
    }

    public function killMysqlTunnel()
    {
        if ($this->sshControlSocket) {
            $cmd = [
                "ssh",
                "-p{$this->port}",
                "-o StrictHostKeyChecking=no",
                "-S {$this->sshControlSocket}",
                "-O exit",
                "{$this->username}@{$this->host}"
            ];
            $cmd = implode(" ", $cmd);
            shell_exec($cmd);
            $this->sshControlSocket = null;
        }
    }

    /**
     * @return SSH2
     */
    protected function ssh()
    {
        if (!$this->ssh) {
            $this->ssh = new SSH2($this->host, $this->port);

            $agent = new Agent();
            $agent->startSSHForwarding(null);
            if (!$this->ssh->login($this->username, $agent)) {
                throw new \RuntimeException('Login failed');
            }
        }

        return $this->ssh;
    }

    /**
     * @return SFTP
     */
    protected function sftp()
    {
        if (!$this->sftp) {
            $this->sftp = new SFTP($this->host, $this->port);

            $agent = new Agent();
            if (!$this->sftp->login($this->username, $agent)) {
                throw new \RuntimeException('Login failed');
            }
        }

        return $this->sftp;
    }

    /**
     * @return string
     */
    public function getTmpDir()
    {
        return $this->exec('echo ~');
    }

    /**
     * @return string
     */
    public function getCurrentDirectoryName()
    {
        return $this->exec('basename "$PWD"');
    }

    public function getCurrentDirectoryPath()
    {
        return false;
    }

    public function chdir($path)
    {
        return false;
    }

    public function readDir($path, $fileMask = '*')
    {
        return false;
    }

}