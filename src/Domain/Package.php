<?php
// vim: set ts=4 sw=4 sts=4 et:

/**
 * Copyright (c) 2011-present Qualiteam software Ltd. All rights reserved.
 * See https://www.x-cart.com/license-agreement.html for license details.
 */

namespace XcartTools\Domain;

use BadMethodCallException;
use Symfony\Component\Filesystem\Filesystem;
use UnexpectedValueException;
use XcartTools\Domain\Provider\ModuleInfo;
use XcartTools\PackageException;

class Package
{
    const HASH_FILE = '.hash';

    /**
     * @var string
     */
    private $packagesDir;

    /**
     * @var string
     */
    private $rootDir;

    /**
     * @var Filesystem
     */
    private $fileSystem;

    /**
     * @var Module
     */
    private $module;

    /**
     * @var boolean
     */
    private $canCompress;

    /**
     * @var ModuleInfo
     */
    private $moduleInfoProvider;

    /**
     * @param string     $rootDir
     * @param string     $tmpDir
     * @param Filesystem $fileSystem
     * @param ModuleInfo $moduleInfoProvider
     */
    public function __construct(
        $rootDir,
        $tmpDir,
        Filesystem $fileSystem,
        ModuleInfo $moduleInfoProvider
    ) {
        $this->packagesDir        = $tmpDir;
        $this->rootDir            = $rootDir;
        $this->fileSystem         = $fileSystem;
        $this->moduleInfoProvider = $moduleInfoProvider;

        $this->canCompress = \Phar::canCompress(\Phar::GZ);
    }

    /**
     * @param Module $module
     *
     * @return self
     */
    public function fromModule($module): self
    {
        $package = clone $this;

        $package->module = $module;

        return $package;
    }

    /**
     * @return string
     */
    public function createPackage(): string
    {
        if ($this->module === null) {
            return '';
        }

        $iterator = $this->getIterator();
        $fullPath = $this->packagesDir . $this->getFileName();

        $this->fileSystem->mkdir($this->packagesDir);
        $this->fileSystem->remove($fullPath);

        $phar = new \PharData($fullPath);
        $phar->buildFromIterator($iterator, $this->rootDir);

        $phar->setMetadata($this->module->toPackageMetadata());

        $phar->addFromString(static::HASH_FILE, json_encode($this->getHash($iterator)));

        if ($this->canCompress) {
            $phar = $phar->compress(\Phar::GZ);
            // Truncates version, see https://bugs.php.net/bug.php?id=58852
            $this->fileSystem->rename($phar->getPath(), $fullPath, true);
        }

        return $fullPath;
    }

    /**
     * @return string
     */
    public function getFileName(): string
    {
        $fileName = $this->module
            ? $this->module->id . '.' . $this->module->version
            : '';

        return $fileName . ($this->canCompress ? '.tgz' : '.tar');
    }

    /**
     * @return array
     */
    public function getModuleFilesList(): array
    {
        return $this->getFilesStructure(
            $this->getIterator()
        );
    }

    /**
     * @return \AppendIterator
     */
    private function getIterator(): \AppendIterator
    {
        $result = new \AppendIterator();

        $moduleInfo = $this->moduleInfoProvider->getModuleInfo($this->module->id);

        if (isset($moduleInfo['directories'])) {
            foreach ((array) $moduleInfo['directories'] as $directory) {
                $result->append(new \RecursiveIteratorIterator(
                    new \RecursiveDirectoryIterator($directory, \FilesystemIterator::SKIP_DOTS)
                ));
            }
        }

        return $result;
    }

    /**
     * @param \Iterator $iterator
     *
     * @return array
     */
    private function getHash(\Iterator $iterator): array
    {
        $files = $this->getFilesStructure($iterator);

        return array_map('md5_file', $files);
    }

    /**
     * @param \Iterator $iterator
     *
     * @return array
     */
    private function getFilesStructure(\Iterator $iterator): array
    {
        $result = [];

        foreach ($iterator as $filePath => $fileInfo) {
            $path = $this->fileSystem->makePathRelative(dirname($filePath), $this->rootDir);

            $result[$path . basename($filePath)] = $filePath;
        }

        return $result;
    }
}