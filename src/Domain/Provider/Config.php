<?php
// vim: set ts=4 sw=4 sts=4 et:

/**
 * Copyright (c) 2011-present Qualiteam software Ltd. All rights reserved.
 * See https://www.x-cart.com/license-agreement.html for license details.
 */

namespace XcartTools\Domain\Provider;


use Exception;
use http\Exception\RuntimeException;
use XcartTools\Context\ContextInterface;
use XcartTools\Context\LocalContext;
use XcartTools\Context\RemoteOverSshContext;
use function implode;

class Config
{
    const DEFAULT_MYSQL_PORT = 3306;

    protected $dir;
    protected $options;
    protected $context;

    /**
     * Config constructor.
     *
     * @param string $configDir
     * @param ContextInterface $context
     */
    public function __construct($configDir, ContextInterface $context)
    {
        $this->dir = $configDir;
        $this->context = $context;
    }

    /**
     * @return string
     * @throws Exception
     */
    public function getDsn()
    {
        if (!$this->hasDbConfiguration()) {
            throw new RuntimeException('Cannot create DSN (no database name), check your config');
        }

        $parts = [
            'mysql:dbname=' . $this->getOption(['database_details', 'database']),
        ];

        if ($this->context instanceof RemoteOverSshContext) {
            $this->context->setUpMysqlTunnel($this->getDbPort());
            $parts[] = 'host=127.0.0.1';
            $parts[] = 'port=' . RemoteOverSshContext::TUNNEL_MYSQL_PORT;
        } else {
            if ($this->isSocketBasedDbConnection()) {
                $parts[] = 'unix_socket=' . $this->getDbSocket();
            } else {
                $parts[] = 'host=' . $this->getOption(['database_details', 'hostspec']);
                $parts[] = 'port=' . $this->getOption(['database_details', 'port']);
            }
        }

        return implode(';', $parts);
    }

    /**
     * @return bool
     * @throws Exception
     */
    public function isSocketBasedDbConnection()
    {
        return (bool)$this->getDbSocket();
    }

    /**
     * @return bool
     */
    public function hasDbConfiguration()
    {
        return !empty($this->getOption(['database_details', 'database']));
    }

    /**
     * @return string
     * @throws Exception
     */
    public function getDbSocket()
    {
        return $this->getOption(['database_details', 'socket']);
    }

    /**
     * @return string
     * @throws Exception
     */
    public function getDbPort()
    {
        return $this->getOption(['database_details', 'port']) ?: static::DEFAULT_MYSQL_PORT;
    }

    /**
     * @return string
     * @throws Exception
     */
    public function getDbUser()
    {
        return $this->getOption(['database_details', 'username']);
    }

    public function getDbName()
    {
        return $this->getOption(['database_details', 'database']);
    }

    public function getDbPass()
    {
        return $this->getOption(['database_details', 'password']);
    }

    public function getDbPrefix()
    {
        return $this->getOption(['database_details', 'table_prefix']);
    }

    /**
     * @param array $path
     *
     * @return mixed
     * @throws Exception
     */
    public function getOption($path)
    {
        $node = $this->getOptions();
        $value = null;
        $abort = false;
        do {
            $key = array_shift($path);
            if (isset($node[$key])) {
                $node = $value = $node[$key];
            } else {
                $abort = true;
                $value = null;
            }
        } while (count($path) > 0 && !$abort);

        return $value;
    }

    /**
     * @return array|null
     * @throws Exception
     */
    public function getOptions()
    {
        if (!$this->options) {
            $options = [];
            $files = array_map(function ($filename) {
                return $this->dir . $filename;
            }, $this->getConfigFilenames());

            foreach ($files as $file) {
                $parsed = $this->parseConfig($file);

                if (is_array($parsed)) {
                    /** @noinspection SlowArrayOperationsInLoopInspection */
                    $options = array_replace_recursive($options, $parsed);
                }
            }

            $this->options = $options;
        }

        return $this->options;
    }

    /**
     * @return string[]
     */
    protected function getConfigFilenames()
    {
        return [
            'config.default.php',
            'config.dev.php',
            'config.php',
            'config.personal.php',
            'config.local.php'
        ];
    }

    /**
     * @param string $path
     * @return array|false
     */
    protected function parseConfig($path)
    {
        if (!$this->context->exists($path)) {
            return false;
        }

        $contents = $this->context->read($path);

        return parse_ini_string($contents, true);
    }

    public function setContext(ContextInterface $context)
    {
        $this->context = $context;
    }
}