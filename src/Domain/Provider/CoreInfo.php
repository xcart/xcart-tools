<?php
// vim: set ts=4 sw=4 sts=4 et:

/**
 * Copyright (c) 2011-present Qualiteam software Ltd. All rights reserved.
 * See https://www.x-cart.com/license-agreement.html for license details.
 */

namespace XcartTools\Domain\Provider;


use Symfony\Component\Filesystem\Filesystem;
use XcartTools\Context\ContextInterface;

class CoreInfo
{
    /**
     * @var string
     */
    private $rootDir;

    /**
     * @var string
     */
    private $versionCache;
    /**
     * @var ContextInterface
     */
    private $context;

    /**
     * @param string           $rootDir
     * @param ContextInterface $context
     */
    public function __construct(
        $rootDir,
        ContextInterface $context
    ) {
        $this->rootDir    = $rootDir;
        $this->context    = $context;
    }

    /**
     * Returns X-Cart core version string in format of 5.x.x.x
     */
    public function getCoreVersion()
    {
        $xliteFile = $this->rootDir . 'classes/XLite.php';

        if (!$this->versionCache && $this->context->exists($xliteFile)) {
            $contents           = $this->context->read($xliteFile);
            $this->versionCache = $this->parseVersionFromXliteFile($contents);
        }

        return $this->versionCache;
    }

    /**
     * @return bool
     */
    public function isNewModuleSystemUsed()
    {
        return version_compare($this->getCoreVersion(), '5.4.0.0') >= 0;
    }

    /**
     * @return bool
     */
    public function isInAppMarketplaceAvailable()
    {
        return version_compare($this->getCoreVersion(), '5.4.1.0') >= 0;
    }

    /**
     * @param $contents
     *
     * @return string
     */
    private function parseVersionFromXliteFile($contents)
    {
        $versionRe = '/const XC_VERSION = [\'"](.*)[\'"];/m';

        $matches = [];

        if (preg_match($versionRe, $contents, $matches)) {
            return $matches[1];
        }

        return null;
    }
}