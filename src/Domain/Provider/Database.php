<?php
// vim: set ts=4 sw=4 sts=4 et:

/**
 * Copyright (c) 2011-present Qualiteam software Ltd. All rights reserved.
 * See https://www.x-cart.com/license-agreement.html for license details.
 */

namespace XcartTools\Domain\Provider;

use PDO;
use function str_replace;
use const PHP_EOL;

class Database
{
    protected $connection;
    protected $prefix;
    protected $database;

    public function __construct(Config $configReader)
    {
        $this->database = $configReader->getDbName();
        $this->prefix   = $configReader->getDbPrefix();

        try {
            $dsn = $configReader->getDsn();
            $this->connection = new PDO(
                $dsn,
                $configReader->getDbUser(),
                $configReader->getDbPass(),
                [
                    PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
                ]);

            if ($this->connection) {
                echo '[OK] DB is connected at: ' . $dsn . PHP_EOL;
            }
        } catch (\Exception $e) {
            echo $e->getMessage() . PHP_EOL;
        }
    }

    /**
     * @return PDO
     * @throws \Exception
     */
    public function getConnection()
    {
        if (!$this->connection) {
            throw new \Exception('No connection to DB');
        }

        return $this->connection;
    }

    /**
     * @param      $statement
     *
     * @param null $params
     * @param bool $prepare
     *
     * @return bool|\PDOStatement
     * @throws \Exception
     */
    public function query($statement, $params = null, $prepare = false)
    {
        $statement = str_replace(['{{prefix}}', '{{database}}'], [$this->prefix, $this->database], $statement);

        if ($prepare) {
            $pdoStmt = $this->getConnection()->prepare($statement);
            $pdoStmt->execute($params);

            return $pdoStmt;
        } else {
            return $this->getConnection()->query($statement);
        }
    }

    public function getTableSchema($table)
    {
        $database = $this->database;
        $query    = <<<QUERY
SELECT
    ORDINAL_POSITION, COLUMN_NAME, IS_NULLABLE, COLUMN_DEFAULT, DATA_TYPE, EXTRA
FROM
     INFORMATION_SCHEMA.COLUMNS
WHERE
    TABLE_NAME = '{{prefix}}${table}' AND TABLE_SCHEMA = '${database}';
QUERY;

        $result = $this->query($query)->fetchAll(PDO::FETCH_ASSOC);

        return $result;
    }
}