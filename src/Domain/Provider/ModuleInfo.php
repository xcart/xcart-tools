<?php
// vim: set ts=4 sw=4 sts=4 et:

/**
 * Copyright (c) 2011-present Qualiteam software Ltd. All rights reserved.
 * See https://www.x-cart.com/license-agreement.html for license details.
 */

namespace XcartTools\Domain\Provider;


use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Yaml\Parser;
use XcartTools\Context\ContextInterface;
use XcartTools\Domain\Module;

class ModuleInfo
{
    /**
     * @var array
     */
    private static $skinModel = [
        'admin'    => ['admin' => []],
        'customer' => ['customer' => []],
        'console'  => ['console' => []],
        'mail'     => ['mail' => ['customer', 'common', 'admin']],
        'common'   => ['common' => []],
        'pdf'      => ['pdf' => ['customer', 'common', 'admin']],
    ];

    /**
     * @var CoreInfo
     */
    private $coreInfo;

    /**
     * @var string
     */
    private $rootDir;

    /**
     * @var array
     */
    private $cache = [];

    /**
     * @var Parser
     */
    private $parser;

    /**
     * @var ContextInterface
     */
    private $context;

    /**
     * @param string           $rootDir
     * @param ContextInterface $context
     * @param CoreInfo         $coreInfo
     * @param Parser           $parser
     */
    public function __construct(
        $rootDir,
        ContextInterface $context,
        CoreInfo $coreInfo,
        Parser $parser
    ) {
        $this->rootDir    = $rootDir;
        $this->coreInfo   = $coreInfo;
        $this->parser     = $parser;
        $this->context = $context;
    }

    /**
     * Fields:
     * id
     * version
     * type
     * author
     * name
     * authorName
     * moduleName
     * description
     * minorRequiredCoreVersion
     * dependsOn
     * incompatibleWith
     * skins
     * showSettingsForm
     * isSystem
     * canDisable
     *
     * directories
     *
     * @param string $moduleId
     *
     * @return array
     */
    public function getModuleInfo($moduleId): array
    {
        if (empty($this->cache[$moduleId])) {
            [$author, $name] = Module::explodeModuleId($moduleId);

            $skins   = [];
            $version = '5.0.0.0';

            $classesDir = $this->rootDir . 'classes/XLite/Module/' . $author . '/' . $name . '/';
            $mainFile   = $this->coreInfo->isNewModuleSystemUsed()
                ? $classesDir . 'main.yaml'
                : $classesDir . 'Main.php';

            $authorName = $author;
            $moduleName = $name;
            $description = '';
            $minorRequiredCoreVersion = 0;
            $dependsOn = [];

            if ($this->context->exists($mainFile)) {
                $contents = $this->context->read($mainFile);
                if ($this->coreInfo->isNewModuleSystemUsed()) {
                    $data    = $this->parser->parse($contents);
                    $version = $data['version'];
                    $skins   = $data['skins'];
                    $authorName = $data['authorName'];
                    $moduleName = $data['moduleName'];
                    $description = $data['description'];
                    $minorRequiredCoreVersion = $data['minorRequiredCoreVersion'];
                    $dependsOn = $data['dependsOn'];
                } else {
                    $version  = $this->parseVersionFromMainPhp($contents);
                    $skins    = $this->parseSkinsFromMainPhp($contents);
                    $authorName = $this->parseAuthorNameFromMainPhp($contents);
                    $moduleName = $this->parseModuleNameFromMainPhp($contents);
                    $minorRequiredCoreVersion = $this->parseMinVersionFromMainPhp($contents) ?: 0;
                    $description = $this->parseDescriptionFromMainPhp($contents);
                    $dependsOn = $this->parseDependenciesFromMainPhp($contents) ?: [];
                }
            }

            $moduleInfo = [
                'id'                       => $moduleId,
                'version'                  => $version,
                'type'                     => 'common',
                'author'                   => $author,
                'name'                     => $name,
                'authorName'               => $authorName,
                'moduleName'               => $moduleName,
                'description'              => $description,
                'minorRequiredCoreVersion' => (int) $minorRequiredCoreVersion,
                'dependsOn'                => $dependsOn,
                'incompatibleWith'         => [],
                'skins'                    => $skins,
                'showSettingsForm'         => false,
                'isSystem'                 => false,
                'canDisable'               => true,
            ];

            $templatesDirs             = $this->getTemplatesDirectories($moduleId);
            $skinsDirs                 = $this->getSkinsDirectories($moduleInfo['skins']);
            $moduleInfo['directories'] = array_merge([$classesDir], $templatesDirs, $skinsDirs);

            $icon = 'classes/XLite/Module/' . $author . '/' . $name . '/icon.png';
            if (!$this->context->readable($this->rootDir . '/' . $icon)) {
                $icon = 'skins/admin/images/addon_default.png';
            }
            $moduleInfo['icon'] = $icon;

            if ($moduleInfo['type'] === 'skin') {
                $preview                   = 'skins/admin/modules/' . $author . '/' . $name . '/preview_list.jpg';
                $moduleInfo['skinPreview'] = $this->context->readable($this->rootDir . '/' . $preview)
                    ? $preview
                    : '';
            }

            $this->cache[$moduleId] = $moduleInfo;
        }

        return $this->cache[$moduleId] ?? [];
    }

    /**
     * @param $contents
     *
     * @return string
     */
    private function parseAuthorNameFromMainPhp($contents)
    {
        return $this->parseStringField('getAuthorName', $contents);
    }

    /**
     * @param $contents
     *
     * @return string
     */
    private function parseMinVersionFromMainPhp($contents)
    {
        return $this->parseStringField('getMinorRequiredCoreVersion', $contents);
    }

    /**
     * @param $contents
     *
     * @return string
     */
    private function parseModuleNameFromMainPhp($contents)
    {
        return $this->parseStringField('getModuleName', $contents);
    }

    /**
     * @param $accessor
     * @param $contents
     * @return string
     */
    private function parseStringField($accessor, $contents)
    {
        $pattern = "/{$accessor}[\s\S]*?return ['\"](.*)['\"]/m";

        if (preg_match($pattern, $contents, $matches)) {
            return $matches[1];
        }

        return "";
    }

    /**
     * @param $accessor
     * @param $contents
     * @return array
     */
    private function parseArrayField($accessor, $contents)
    {
        $code        = null;
        $newSyntaxRe = "/{$accessor}[\s\S]*?return (\[[\s\S]*?\];)/m";
        $oldSyntaxRe = "/{$accessor}[\s\S]*?return (array\([\s\S]*?\);)/m";

        $matches = [];

        if (preg_match($newSyntaxRe, $contents, $matches)
            || preg_match($oldSyntaxRe, $contents, $matches)) {
            $code = 'return ' . $matches[1];

            return @eval($code);
        }

        return [];
    }

    /**
     * @param $contents
     *
     * @return string
     */
    private function parseVersionFromMainPhp($contents)
    {
        $partsRegex = [
            "/getMajorVersion[\s\S]*?return ['\"](.*)['\"]/m",
            "/getMinorVersion[\s\S]*?return ['\"](.*)['\"]/m",
            "/getBuildVersion[\s\S]*?return ['\"](.*)['\"]/m",
        ];

        $parts = [];

        foreach ($partsRegex as $pattern) {
            $matches = [];
            if (preg_match($pattern, $contents, $matches)) {
                $parts[] = $matches[1];
            }
        }

        return implode('.', $parts);
    }

    private function parseDescriptionFromMainPhp($contents)
    {
        $code        = null;
        $syntax = "/getDescription[\s\S]*?return (['\"].*['\"];)/m";

        $matches = [];

        if (preg_match($syntax, $contents, $matches)) {
            $code = 'return ' . $matches[1];

            return @eval($code);
        }

        return "";
    }

    /**
     * @param $contents
     *
     * @return array
     */
    private function parseSkinsFromMainPhp($contents)
    {
        return $this->parseArrayField('getSkins', $contents);
    }

    /**
     * @param $contents
     *
     * @return array
     */
    private function parseDependenciesFromMainPhp($contents)
    {
        return $this->parseArrayField('getDependencies', $contents);
    }

    /**
     * @param string $moduleId
     *
     * @return array
     */
    private function getTemplatesDirectories($moduleId): array
    {
        $result = [];
        foreach (self::$skinModel as $interface => $model) {
            foreach ($model as $dir => $innerDirs) {
                $basePaths = $this->findFoldersStartingWithName($this->rootDir . 'skins', $dir);

                if ($innerDirs) {
                    foreach ($basePaths as $basePath) {
                        foreach ($innerDirs as $innerDir) {
                            $basePaths = array_merge($basePaths, $this->findFoldersStartingWithName($basePath, $innerDir));
                        }
                    }
                }

                $result = array_merge($result, $basePaths);
            }
        }

        [$author, $name] = Module::explodeModuleId($moduleId);
        $modulePath = $author . '/' . $name . '/';

        return array_reduce($result, function ($acc, $item) use ($modulePath) {
            $path = $item . 'modules/' . $modulePath;
            if (is_dir($path)) {
                $acc[] = $path;
            }

            return $acc;
        }, []);
    }

    /**
     * @param array $skins
     *
     * @return array
     */
    private function getSkinsDirectories($skins): array
    {
        $result = [];
        foreach ((array) $skins as $interface => $dirs) {
            foreach ($dirs as $dir) {
                $realDir = $this->rootDir . 'skins/' . $dir . '/';
                if (is_dir($realDir)) {
                    $result[] = $realDir;
                }
            }
        }

        return $result;
    }

    /**
     * @param $root
     * @param $name
     *
     * @return array
     */
    private function findFoldersStartingWithName($root, $name): array
    {
        $result = [];
        foreach (new \DirectoryIterator($root) as $fileInfo) {
            if (!$fileInfo->isDot()
                && $fileInfo->isDir()
                && ($fileInfo->getFilename() === $name || 0 === strpos($fileInfo->getFilename(), $name . '_'))
            ) {
                $result[] = $fileInfo->getPathname() . '/';
            }
        }

        return $result;
    }
}