<?php
// vim: set ts=4 sw=4 sts=4 et:

/**
 * Copyright (c) 2011-present Qualiteam software Ltd. All rights reserved.
 * See https://www.x-cart.com/license-agreement.html for license details.
 */

namespace XcartTools;

use Exception;

class PackageException extends Exception
{
    public static function fromNonPharArchive()
    {
        return new PackageException('Non phar module archive');
    }

    public static function fromGenericError($msg)
    {
        return new PackageException($msg);
    }
}