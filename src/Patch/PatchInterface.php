<?php


namespace XcartTools\Patch;


interface PatchInterface
{
    public function getDiff(): string;
}