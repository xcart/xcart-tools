<?php


namespace XcartTools\Patch;


class RebuildPhp implements PatchInterface
{
    private $patch = <<<'PATCH'
@@ -12,13 +12,13 @@
 define('XCN_ADMIN_SCRIPT', true);
 
 try {
-    require_once __DIR__ . DIRECTORY_SEPARATOR . 'top.inc.rebuild.php';
-
     if (PHP_SAPI === 'cli') {
         $options = getopt('', ['request::']);
         $_REQUEST = json_decode($options['request'], true);
     }

+    require_once __DIR__ . DIRECTORY_SEPARATOR . 'top.inc.rebuild.php';
+
     $rebuild = new \XLite\Rebuild\OldSystemAdapter();
     $rebuild->setRequest($_REQUEST);
PATCH;

    /**
     * @return string
     */
    public function getDiff(): string
    {
        return $this->patch;
    }
}