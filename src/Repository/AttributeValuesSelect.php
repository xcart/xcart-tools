<?php
// vim: set ts=4 sw=4 sts=4 et:

/**
 * Copyright (c) 2011-present Qualiteam software Ltd. All rights reserved.
 * See https://www.x-cart.com/license-agreement.html for license details.
 */

namespace XcartTools\Repository;

class AttributeValuesSelect extends RepositoryAbstract
{
    protected function getTableName()
    {
        return 'attribute_values_select';
    }

    protected function getPrimaryKeyColumn()
    {
        return 'id';
    }
}