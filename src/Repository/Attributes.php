<?php
// vim: set ts=4 sw=4 sts=4 et:

/**
 * Copyright (c) 2011-present Qualiteam software Ltd. All rights reserved.
 * See https://www.x-cart.com/license-agreement.html for license details.
 */

namespace XcartTools\Repository;

use PDO;

class Attributes extends RepositoryAbstract
{
    protected function getTableName()
    {
        return 'attributes';
    }

    protected function getPrimaryKeyColumn()
    {
        return 'id';
    }

    public function findAll()
    {
        $table = $this->getTableName();

        $query = <<<SQL
SELECT * FROM {{prefix}}${table} WHERE product_class_id IS NULL AND product_id IS NULL AND type = 'S'
SQL;

        return $this->database->query($query)->fetchAll(PDO::FETCH_ASSOC);
    }
}