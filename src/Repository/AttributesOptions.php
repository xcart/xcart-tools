<?php
// vim: set ts=4 sw=4 sts=4 et:

/**
 * Copyright (c) 2011-present Qualiteam software Ltd. All rights reserved.
 * See https://www.x-cart.com/license-agreement.html for license details.
 */

namespace XcartTools\Repository;

use PDO;

class AttributesOptions extends RepositoryAbstract
{
    protected function getTableName()
    {
        return 'attribute_options';
    }

    protected function getPrimaryKeyColumn()
    {
        return 'id';
    }

    public function findApplicable($list = [])
    {
        $table = $this->getTableName();

        $clean = array_filter(array_unique(array_map('intval', $list)));

        if (!$clean) {
            return [];
        }
        $prepared = implode(',', $clean);

        $query = <<<SQL
SELECT * FROM {{prefix}}${table} WHERE attribute_id IN (${prepared})
SQL;

        return $this->database->query($query)->fetchAll(PDO::FETCH_ASSOC);
    }
}