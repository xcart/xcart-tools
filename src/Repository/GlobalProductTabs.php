<?php
// vim: set ts=4 sw=4 sts=4 et:

/**
 * Copyright (c) 2011-present Qualiteam software Ltd. All rights reserved.
 * See https://www.x-cart.com/license-agreement.html for license details.
 */

namespace XcartTools\Repository;


class GlobalProductTabs extends RepositoryAbstract
{
    protected function getTableName()
    {
        return 'global_product_tabs';
    }

    protected function getPrimaryKeyColumn()
    {
        return 'id';
    }
}