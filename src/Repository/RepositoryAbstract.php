<?php
// vim: set ts=4 sw=4 sts=4 et:

/**
 * Copyright (c) 2011-present Qualiteam software Ltd. All rights reserved.
 * See https://www.x-cart.com/license-agreement.html for license details.
 */

namespace XcartTools\Repository;

use PDO;
use XcartTools\Domain\Provider\Database;

abstract class RepositoryAbstract
{
    /**
     * @var Database
     */
    protected $database;

    protected abstract function getTableName();
    protected abstract function getPrimaryKeyColumn();

    public function __construct(Database $database)
    {
        $this->database = $database;
    }

    public function getById($id)
    {
        $table = $this->getTableName();
        $key = $this->getPrimaryKeyColumn();

        $query = <<<SQL
SELECT * FROM {{prefix}}${table} WHERE ${key} = ? LIMIT 1
SQL;
        $result = $this->database->query($query, [$id], true)->fetchAll(PDO::FETCH_ASSOC);

        return $result
            ? array_pop($result)
            : null;
    }

    public function findBy($column, $value)
    {
        $table = $this->getTableName();

        $query = <<<SQL
SELECT * FROM {{prefix}}${table} WHERE ${column} = ? LIMIT 1
SQL;
        $result = $this->database->query($query, [$value], true)->fetchAll(PDO::FETCH_ASSOC);

        return $result
            ? array_pop($result)
            : null;
    }

    public function findAll()
    {
        $table = $this->getTableName();

        $query = <<<SQL
SELECT * FROM {{prefix}}${table}
SQL;

        return $this->database->query($query)->fetchAll(PDO::FETCH_ASSOC);
    }

    public function remove($id)
    {
        $table = $this->getTableName();
        $key = $this->getPrimaryKeyColumn();

        $query = <<<SQL
DELETE FROM {{prefix}}${table} WHERE ${key} = ?
SQL;
        $result = $this->database->query($query, [$id], true);

        return $result;
    }

    public function updateById($id, $column, $value)
    {
        $table = $this->getTableName();
        $key = $this->getPrimaryKeyColumn();

        $query = <<<SQL
UPDATE {{prefix}}${table} SET ${column} = ? WHERE ${key} = ?
SQL;

        return $this->database->query($query, [$value, $id], true);
    }

    public function insert($record)
    {
        $record = $this->fillNonNullColumns($record);
        $table = $this->getTableName();

        $columns = $this->prepareColumns($record);
        $values = $this->prepareValues($record);

        $insert = <<<SQL
INSERT INTO {{prefix}}${table} (${columns}) VALUES (${values})
SQL;

        return $this->database->query($insert, null, true)->rowCount() > 0
            ? $this->database->getConnection()->lastInsertId()
            : false;
    }

    public function getNextId()
    {
        $table = $this->getTableName();

        $query = <<<SQL
        SELECT AUTO_INCREMENT FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA = '{{database}}' AND TABLE_NAME = '{{prefix}}${table}'
SQL;

        $result = (int) $this->database->query($query)->fetchColumn(0);

        return $result;
    }

    /**
     * @param array $record
     *
     * @return string
     */
    protected function prepareColumns($record)
    {
        return implode(',', array_map(function ($item) {
            return "$item";
        }, array_keys($record)));
    }

    /**
     * @param array $record
     *
     * @return string
     */
    protected function prepareValues($record)
    {
        return implode(',', array_map(function ($item) {
            return is_numeric($item) ? $item : "'$item'";
        }, array_values($record)));
    }

    protected function fillNonNullColumns($record)
    {
        $schema = $this->database->getTableSchema($this->getTableName());

        foreach ($schema as $info) {
            $key = $info['COLUMN_NAME'];
            if (strpos($info['EXTRA'], 'auto_increment') !== false) {
                continue;
            }

            if ($info['IS_NULLABLE'] === 'NO' && !isset($record[$key])) {
                if (strpos($info['DATA_TYPE'], 'int') !== false) {
                    $record[$key] = 0;
                } elseif (strpos($info['DATA_TYPE'], 'decimal') !== false) {
                    $record[$key] = 0.0;
                } elseif (strpos($info['DATA_TYPE'], 'datetime') !== false) {
                    $record[$key] = '0000-00-00 00:00:00';
                } else {
                    $record[$key] = '';
                }
            }
        }

        return $record;
    }
}