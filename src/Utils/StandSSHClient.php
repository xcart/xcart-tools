<?php
/** @noinspection PhpComposerExtensionStubsInspection */
// vim: set ts=4 sw=4 sts=4 et:

/**
 * Copyright (c) 2011-present Qualiteam software Ltd. All rights reserved.
 * See https://www.x-cart.com/license-agreement.html for license details.
 */

namespace XcartTools\Utils;

use phpseclib\Net\SFTP;
use phpseclib\Net\SSH2;
use phpseclib\System\SSH\Agent;

class StandSSHClient
{
    /**
     * @var SSH2
     */
    protected $ssh;

    /**
     * @var SFTP
     */
    protected $sftp;
    /**
     * @var string
     */
    private $host;
    /**
     * @var string
     */
    private $port;
    /**
     * @var string
     */
    private $username;

    /**
     * @param string $host
     * @param string $port
     * @param string $username
     */
    public function __construct($host, $port, $username)
    {
        $this->host = $host;
        $this->port = $port;
        $this->username = $username;
    }

    public function testConnection()
    {
        return (bool)$this->executeCommand('ls');
    }

    public function executeCommand($command)
    {
        return $this->ssh()->exec($command);
    }

    public function executeMySQLCommand($command)
    {
        return $this->executeCommand("mysql --execute=\"$command\"");
    }

    public function uploadFile($localPath, $remotePath)
    {
        return $this->sftp()->put($remotePath, $localPath, SFTP::SOURCE_LOCAL_FILE);
    }

    public function fileExists($remotePath)
    {
        return $this->sftp()->file_exists($remotePath);
    }

    /**
     * @return SSH2
     */
    protected function ssh()
    {
        if (!$this->ssh) {
            $this->ssh = new SSH2($this->host, $this->port);

            $agent = new Agent();
            $agent->startSSHForwarding(null);
            if (!$this->ssh->login($this->username, $agent)) {
                throw new \RuntimeException('Login failed');
            }
        }

        return $this->ssh;
    }

    /**
     * @return SFTP
     */
    protected function sftp()
    {
        if (!$this->sftp) {
            $this->sftp = new SFTP($this->host, $this->port);

            $agent = new Agent();
            if (!$this->sftp->login($this->username, $agent)) {
                throw new \RuntimeException('Login failed');
            }
        }

        return $this->sftp;
    }
}
