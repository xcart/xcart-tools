<?php
/** @noinspection PhpComposerExtensionStubsInspection */
// vim: set ts=4 sw=4 sts=4 et:

/**
 * Copyright (c) 2011-present Qualiteam software Ltd. All rights reserved.
 * See https://www.x-cart.com/license-agreement.html for license details.
 */

namespace XcartTools\Utils;

class StandSSHClientFactory
{
    /**
     * @param string $host
     * @param string $port
     * @param string $username
     *
     * @return StandSSHClient
     */
    public function create($host, $port, $username)
    {
        return new StandSSHClient($host, $port, $username);
    }
}
