<?php
// vim: set ts=4 sw=4 sts=4 et:

/**
 * Copyright (c) 2011-present Qualiteam software Ltd. All rights reserved.
 * See https://www.x-cart.com/license-agreement.html for license details.
 */

namespace XcartTools\Utils;


use XcartTools\Context\ContextInterface;
use XcartTools\Context\LocalContext;
use XcartTools\Context\RemoteOverSshContext;
use function file_exists;
use function getcwd;

class XcartPathLocator
{
    /**
     * @var string
     */
    protected $xcartPath;

    /**
     * @var ContextInterface
     */
    protected $context;

    /**
     * XcartPathLocator constructor.
     *
     * @param ContextInterface $context
     */
    public function __construct(ContextInterface $context)
    {
        $this->setContext($context);
    }

    /**
     * @param ContextInterface $context
     */
    public function setContext($context)
    {
        $this->context = $context;
        $this->xcartPath = null;
    }

    /**
     * @return string
     */
    public function getXcartPath()
    {
        if (!$this->xcartPath) {
            $paths = $this->getXcartPaths();
            foreach ($paths as $path) {
                if ($this->context->exists($path . '/cart.php')) {
                    $this->xcartPath = $path;
                    break;
                }
            }
        }

        return $this->xcartPath;
    }

    /**
     * @return bool
     */
    public function isRemoteContext()
    {
        return $this->context instanceof RemoteOverSshContext;
    }

    protected function getXcartPaths()
    {
        return [
            $this->context->exec('pwd'),
            $this->context->exec('pwd') . '/src',
            $this->context->exec(['echo', '~']) . '/httpdocs',
            $this->context->exec(['echo', '~']) . '/httpdocs/src',
            $this->context->exec(['echo', '~']) . '/www',
            $this->context->exec(['echo', '~']) . '/www/src',
            '/var/www/html',
            '/var/www/html/src',
            '/var/www/html/deploy',
        ];
    }

    /**
     * @return string
     */
    public function getConfigFilepath()
    {
        return $this->getConfigDir() . 'config.php';
    }

    /**
     * @return string
     */
    public function getConfigDir()
    {
        return $this->getXcartPath() . '/etc/';
    }

    /**
     * @return string
     */
    public function getCustomerScriptPath()
    {
        return $this->getXcartPath() . '/cart.php';
    }

    /**
     * @return string
     */
    public function getRebuildMarkFilepath()
    {
        return $this->getXcartPath() . '/var/.rebuildMark';
    }

    /**
     * @return string
     */
    public function getRebuildStartedFilepath()
    {
        return $this->getXcartPath() . '/var/.rebuildStarted';
    }

    /**
     * @return string
     */
    public function getCacheIndicatorMask()
    {
        return $this->getXcartPath() . '/var/run/.cacheGenerated.*.step';
    }

    /**
     * @return string
     */
    public function getScriptStateMask()
    {
        return $this->getXcartPath() . '/files/service/scriptStateStorage*';
    }

    /**
     * @return string
     */
    public function getModulePath($authorId, $moduleId)
    {
        return $this->getXcartPath() . '/classes/XLite/Module/' . $authorId . '/' . $moduleId;
    }

    /**
     * @return string
     */
    public function getModuleMainYamlPath($authorId, $moduleId)
    {
        return $this->getXcartPath() . '/classes/XLite/Module/' . $authorId . '/' . $moduleId . '/main.yaml';
    }

    /**
     * @return string
     */
    public function getModuleInstallYamlPath($authorId, $moduleId)
    {
        return $this->getXcartPath() . '/classes/XLite/Module/' . $authorId . '/' . $moduleId . '/install.yaml';
    }

    /**
     * @return string
     */
    public function getXcartVarPath()
    {
        return $this->getXcartPath() . '/var';
    }

    /**
     * @return string
     */
    public function getXcartFilesServicePath()
    {
        return $this->getXcartPath() . '/files/service';
    }
}