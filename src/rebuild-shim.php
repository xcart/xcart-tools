<?php
// vim: set ts=4 sw=4 sts=4 et:

/**
 * Copyright (c) 2011-present Qualiteam software Ltd. All rights reserved.
 * See https://www.x-cart.com/license-agreement.html for license details.
 */

use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\HttpKernelInterface;

if ('cli' !== PHP_SAPI) {
    exit(1);
}

$options = getopt('', ['action::', 'dir::']);

chdir($options['dir']);

require_once './service/vendor/autoload.php';
require_once './modules_manager/autoload.php';

$config = new XCart\ConfigParser\ConfigParser($_SERVER, $options['dir'] . '/etc/');

$appFactory = require './service/src/App.php';

/** @var \Silex\Application $app */
$app = $appFactory($config->getData());

$options['auth_code'] = $app['config']['authcode_reference'];
$request  = Request::create('/rebuild', 'GET', $options);

try {
    $response = $app->handle($request, HttpKernelInterface::MASTER_REQUEST, false);

    if ($response instanceof RedirectResponse) {
        $regex = "/service\.php\?#\/rebuild\/(scenario.*)\?scode/m";
        $matches = [];
        preg_match($regex, $response->getTargetUrl(), $matches);
        $response = new Response($matches[1], 200);

        $response->send();
        $app->terminate($request, $response);
    } else {
        throw new Exception($response->getContent());
    }

} catch (Exception $e) {
    $response = new Response($e->getMessage(), 500);
    $response->send();
    $app->terminate($request, $response);
    exit(1);
}
